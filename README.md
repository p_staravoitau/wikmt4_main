# README #

###  ###

* Запросы на которые не написаны тесты: 'getaccountswithpl1' 

### Для начала работы необходимо: ###

* установить [Allure Framework]
* для тестов потребуется также запуск секретной сборки WIK 
* в обеих сборка необходимо установить socketTimeout=100
* заполнить файл config.py
* установить необходимые расширения из requirements.txt: `pip install -r requirements.txt `
* для проверки запросов: deletepluginvalue, getplugin, updatepluginvalue - необходимо установить uber_feed.dll
* в секретной сборке WIK установить reboot_timeout=100000
* для символа EURUSD установить 5 знаков после запятой в настройках МТ4

### Запросы, которые могут потребоваться для запуска теста и просмотре отчёта: ###

* pytest --alluredir=allure_results - запуск тестов с генерацией отчёта в allure
* pytest --lf - запуск недавно проваленных тестов
* pytest -m smoke - запуск smoke-тестирования
* allure serve allure_results - просмотр отчётов allure

### Автоматическая отправка отчета Allure: ###

* Для автомотической отправки отчета Allure на Cloud, неообходимо запустить модуль ALLURE_UPLOAD.py

[Allure Framework]: https://docs.qameta.io/allure/#_installing_a_commandline