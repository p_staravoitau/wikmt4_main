import web_api_mt4 as wik4
import pytest
import allure
import time
from config import wik_mt4_addr, wik_mt4_addr_secret


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        time.sleep(0.1)


@allure.title("Запрос deleteorder: удаление одного ордера")
@allure.severity('critical')
def test_deleteorder_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос changebalance на пополнение баланса 101 аккаунту
            Шаг2: Отправить запрос deleteorder для удаления ордера
            Шаг3: Отправить запрос gethistory для проверки, что ордер действительно был удален
        Tear down: Удалить аккаунт
    """

    with allure.step('Запрос changebalance на пополнение баланса 101 аккаунту'):
        changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000', comment='test_deleteorder')
        print('Ответ Шаг1:\n', changebalance_1)
        with allure.step('Проверка ответа'):
            assert '1' == changebalance_1['result']
    with allure.step('Запрос deleteorder для удаления ордера'):
        deleteorder_1 = wik4.request(wik_mt4_addr, 'deleteorder', order=changebalance_1['orderid'])
        print('Ответ Шаг2:\n', deleteorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == deleteorder_1['result']
    with allure.step('Запрос gethistory: проверка, что ордер действительно был удален'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        gethistory_1 = wik4.request(wik_mt4_addr, 'gethistory', **{'login':'101', 'from':int(current_time_1['time'])-1000, 'to': current_time_1['time']})
        print('Ответ Шаг3:\n', gethistory_1)
        with allure.step('Проверка ответа'):
            assert '1' == gethistory_1['result'] and '' == gethistory_1['answer'][0]['login']



@allure.title("Запрос deleteorder: удаление нескольких ордеров")
@allure.severity('critical')
def test_deleteorder_2(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить два запроса changebalance на пополнение баланса 101 аккаунту
            Шаг2: Отправить запрос deleteorder для удаления нескольких ордеров
            Шаг3: Отправить запрос gethistory для проверки, что ордера действительно былы удалены
        Tear down: Удалить аккаунт
    """

    with allure.step('Запрос changebalance на пополнение баланса 101 аккаунту'):
        changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000', comment='test_deleteorder')
        changebalance_2 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000',
                                       comment='test_deleteorder')
        print('Ответ Шаг1:\n', changebalance_1, changebalance_2)
        with allure.step('Проверка ответа'):
            assert '1' == changebalance_1['result'] and '1' == changebalance_2['result']
    with allure.step('Запрос deleteorder для удаления ордера'):
        deleteorder_1 = wik4.request(wik_mt4_addr, 'deleteorder', order=changebalance_1['orderid'] + ';' + changebalance_2['orderid'])
        print('Ответ Шаг2:\n', deleteorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == deleteorder_1['result']
    with allure.step('Запрос gethistory: проверка, что ордер действительно был удален'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        gethistory_1 = wik4.request(wik_mt4_addr, 'gethistory', **{'login':'101', 'from':int(current_time_1['time'])-1000, 'to': current_time_1['time']})
        print('Ответ Шаг3:\n', gethistory_1)
        with allure.step('Проверка ответа'):
            assert '1' == gethistory_1['result'] and '' == gethistory_1['answer'][0]['login']


@allure.title("Запрос deleteorder: удаление нескольких ордеров, один из которых - несуществующий")
@allure.severity('normal')
def test_deleteorder_3(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос changebalance на пополнение баланса 101 аккаунту
            Шаг2: Отправить запрос deleteorder для удаления ордера (один существующий, другой - нет)
            Шаг3: Отправить запрос gethistory для проверки, что ордер действительно был удален
            Ожидаемый ответ: result=1&size=6\n777777&request_id=
        Tear down: Удалить аккаунт
    """

    with allure.step('Запрос changebalance на пополнение баланса 101 аккаунту'):
        changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000', comment='test_deleteorder')
        print('Ответ Шаг1:\n', changebalance_1)
        with allure.step('Проверка ответа'):
            assert '1' == changebalance_1['result']
    with allure.step('Запрос deleteorder для удаления ордера (один существующий, другой - нет)'):
        deleteorder_1 = wik4.request(wik_mt4_addr, 'deleteorder', order=changebalance_1['orderid']+";777777")
        print('Ответ Шаг2:\n', deleteorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == deleteorder_1['result'] and '777777' == deleteorder_1['answer'][0]['order_no_delete']
    with allure.step('Запрос gethistory: проверка, что ордер действительно был удален'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        gethistory_1 = wik4.request(wik_mt4_addr, 'gethistory', **{'login':'101', 'from':int(current_time_1['time'])-1000, 'to': current_time_1['time']})
        print('Ответ Шаг3:\n', gethistory_1)
        with allure.step('Проверка ответа'):
            assert '1' == gethistory_1['result'] and '' == gethistory_1['answer'][0]['login']


@allure.title("Запрос deleteorder: удаление неcуществующего ордера")
@allure.severity('normal')
def test_deleteorder_4():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос deleteorder для удаления несущестсующего ордера
            Ожидаемый ответ: result=-4&reason=no trades&request_id=
        Tear down: -
    """

    reason = 'no trades'
    with allure.step('Запрос deleteorder для удаления несуществующего ордера'):
        deleteorder_1 = wik4.request(wik_mt4_addr, 'deleteorder', order=777777)
        print('Ответ Шаг1:\n', deleteorder_1)
        with allure.step('Проверка ответа'):
            assert '-4' == deleteorder_1['result'] and reason == deleteorder_1['reason']