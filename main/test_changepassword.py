import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100,
                     password_investor='123456Qw', password='Qw123456')
    time.sleep(.100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)



@allure.title("Запрос changepassword: несуществующий логин.")
@allure.severity('minor')
def test_changepassword_1():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос changepassword с несуществующим аккаунтом
            Ожидаемый результат: result=-4&reason=no valid data on mt4 server
        Tear down:  -
    """

    with allure.step('Запрос changepassword с несуществующим логином'):
        changepassword_1 = wik4.request(wik_mt4_addr, 'changepassword', **{'login': '101', 'investor': '0', 'pass': '1234Pass'})
        print('Ответ Шаг1:\n', changepassword_1)
        with allure.step('Проверка ответа'):
            assert changepassword_1['result'] == '-4'


@allure.title("Запрос changepassword: короткий master пароль")
@allure.severity('critical')
def test_changepassword_2(accounts):
    """
        Описание шагов:
            Setup: Создать аккаунт 101.
            Тест:
                  Шаг1: Отправить запрос changepassword с pass = q2
                  Ожидаемый ответ: result=-3&mt4_ret=3&reason=Invalid parameters&login=101&request_id=
            Tear down: удаление аккаунта
    """
    reason = 'Invalid parameters'
    with allure.step('Запрос changepassword с коротким паролем'):
        changepassword_1 = wik4.request(wik_mt4_addr, 'changepassword', **{'login': '101', 'pass': 'q2', 'investor': '0'})
        print('Ответ Шаг1:\n', changepassword_1)
        with allure.step('Проверка ответа'):
            assert changepassword_1['result'] == '-3' and reason == changepassword_1['reason']


@allure.title("Запрос changepassword: изменение master пароля на аккаунте")
@allure.severity('normal')
def test_changepassword_3(accounts):
    """
        Описание шагов:
            Setup: Создаём аккаунт 101.
            Тест:
                Шаг1: Запрос changepassword на изменения мастер пароля аккаунту 101
                Шаг2: Запрос на проверку того, что пароль был изменен
            Tear down: Удалить аккаунт
    """

    with allure.step('Запрос changepassword на изменения мастер пароля аккаунту 101'):
        changepassword_1 = wik4.request(wik_mt4_addr, 'changepassword', **{'login': '101', 'pass': '1234Pass', 'investor': '0'})
        print('Ответ Шаг1:\n', changepassword_1)
        with allure.step('Проверка ответа'):
            assert changepassword_1['result'] == '1'
    with allure.step('Запрос на проверку того, что пароль был изменен'):
        checkpassword_2 = wik4.request(wik_mt4_addr, 'checkpassword', **{'login': '101', 'pass': '1234Pass'})
        print('Ответ Шаг2:\n', checkpassword_2)
        with allure.step('Проверка ответа'):
            assert checkpassword_2['result'] == '1'

@allure.title("Запрос changepassword: отсутствие одного из обязательных параметров")
@allure.severity('normal')
def test_changepassword_4(accounts):
    """
        Описание шагов:
            Setup: Создаём аккаунт 101.
            Тест:
                Шаг1: Запрос changepassword с отсутствующим параметром login
                Шаг2: Запрос changepassword с отсутствующим параметром pass
                Шаг3: Запрос changepassword с отсутствующим параметром investor
                Ожидаемые ответ: result=-3&mt4_ret=3&reason=Invalid parameters&login=101&request_id=
            Tear down: Удалить аккаунт
    """
    reason = 'Invalid parameters'
    with allure.step('Запрос changepassword с отсутствующим параметром login'):
        changepassword_1 = wik4.request(wik_mt4_addr, 'changepassword', **{'pass': '1234Pass', 'investor': '1'})
        print('Ответ Шаг1:\n', changepassword_1)
        with allure.step('Проверка ответа'):
            assert changepassword_1['result'] == '-3' and reason == changepassword_1['reason']
    with allure.step('Запрос changepassword с отсутствующим параметром pass'):
        changepassword_2 = wik4.request(wik_mt4_addr, 'changepassword', **{'login': '101', 'investor': '1'})
        print('Ответ Шаг2:\n', changepassword_2)
        with allure.step('Проверка ответа'):
            assert changepassword_2['result'] == '-3' and reason == changepassword_2['reason']
    with allure.step('Запрос changepassword с отсутствующим параметром investor'):
        changepassword_3 = wik4.request(wik_mt4_addr, 'changepassword', **{'login': '101', 'pass': '1234Pass'})
        print('Ответ Шаг3:\n', changepassword_3)
        with allure.step('Проверка ответа'):
            assert changepassword_3['result'] == '-3' and reason == changepassword_3['reason']
