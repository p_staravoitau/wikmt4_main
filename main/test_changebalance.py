import web_api_mt4 as wik4
import pytest
import time
import allure
import random
from config import wik_mt4_addr, wik_mt4_addr_secret



@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        time.sleep(0.1)


@allure.title("Запрос changebalance: Попытка списать в минус.")
@allure.severity('critical')
def test_changebalance_1(accounts):
    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changebalance на списание с аккаунта 10.000.
            Шаг2: Запросить getaccountbalance баланс аккаунта.
            Шаг3: Проверить ответ.
            Ожидаемый ответ:
            Шаг 1: result=-4&reason=after operation balance will be negative&login=101&request_id=
            Шаг 2: result=1&login=101&balance=0.00&request_id=
        Tear down: удалить аккаунт
    """
    reason ='after operation balance will be negative'
    with allure.step('Списать баланс 101 аккаунта в минус'):
        changebalance_minus = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='-10000', comment='test_changebalance')
        print('Ответ Шаг1:\n', changebalance_minus)
        time.sleep(1)
    with allure.step('Проверка списания баланса 101 аккаунта'):
        getaccountbalance_0 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
        print('Ответ Шаг2:\n', getaccountbalance_0)
        with allure.step('Проверка ответа'):
            assert float(0) == float(getaccountbalance_0['balance']) and '-4' == changebalance_minus['result'] \
                   and reason == changebalance_minus['reason']


@allure.title("Запрос changebalance: Начисление и списание баланса.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_changebalance_2(accounts):

    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changebalance на пополнение аккаунта.
            Шаг2: Запросить getaccountbalance баланс аккаунта.
            Шаг3: Проверить ответ.
            Шаг4: Отправить запрос changebalance на списание с аккаунта.
            Шаг5: Запросить getaccountbalance баланс аккаунта.
            Шаг6: Проверить ответ.
        Tear down: Удалить аккаунт.
    """

    with allure.step('Пополнить баланс 101 аккаунта запросом changebalance.'):
        changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value=10000, comment='test_changebalance')
        print('Ответ Шаг1:\n', changebalance_1)
        time.sleep(.100)
        with allure.step('Проверить зачисление средств на баланс'):
            getaccountbalance_0 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
            print('Ответ Шаг2:\n', getaccountbalance_0)
            with allure.step('Проверка ответов.'):
                assert float(10000) == float(getaccountbalance_0['balance'])
    with allure.step('Списание баланса запросом changebalance.'):
        changebalance_2 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value="-555.55", comment='test_changebalance')
        print('Ответ Шаг4:\n', changebalance_2)
        time.sleep(.100)
        with allure.step('Проверка списания средств с баланса'):
            getaccountbalance_1 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
            print('Ответ Шаг5:\n', getaccountbalance_1)
            with allure.step('Проверка ответов.'):
                assert float(10000-555.55) == float(getaccountbalance_1['balance'])



@allure.title("Запрос changebalance: несуществующий аккаунт.")
@allure.severity('minor')
def test_changebalance_3():
    """
    Описание шагов:
        Тест:
            Шаг1: Отправить запрос changebalance на пополнение несуществующего аккаунта
            Шаг2: Проверить ответ.
            Ожидаемый ответ: result=-4&mt4_ret=3&reason=Invalid parameters&login=101&request_id=
    """
    reason ="Invalid parameters"
    with allure.step('Запрос changebalance на пополнение несуществующего аккаунта'):
        changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='10000',
                                                   comment='test_changebalance')
        print('Ответ Шаг1:\n', changebalance_1)
        assert '-4' == changebalance_1['result'] and reason == changebalance_1['reason']


@allure.title("Запрос changebalance: пустой комментарий.")
@allure.severity('normal')
def test_changebalance_4(accounts):
    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changebalance с пустым комментарием.
            Шаг2: Проверить ответ.
            Ожидаемый ответ: result=-3&reason=invalid params&login=101&request_id=
        Tear down: Удалить аккаунт
    """
    reason ='invalid params'
    with allure.step('Запрос changebalance с пустым комментарием.'):
        changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value=10000, comment='')
        print('Ответ Шаг1:\n', changebalance_1)
        with allure.step('Проверка ответа'):
            assert '-3' == changebalance_1['result'] and reason == changebalance_1['reason']


@allure.title("Запрос changebalance: с опциональным параметром freemarginlimit")
@allure.severity('minor')
def test_changebalance_5(accounts):
    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changebalance  на пополнение 100.000 аккауниу 101.
            Шаг2: Запрос changebalance с опц. параметром freemarginlimit, снятие невозможно
            Шаг3: Запрос changebalance с опц. параметром freemarginlimit, снятие возможно
            Ожидаемый ответ:
            Шаг1: result=1&login=101&newbalance=100000.00&orderid=589536&request_id=
            Шаг2: result=-3&reason=OK&login=101&request_id=
            Шаг3: result=1&login=101&newbalance=10000.00&orderid=589544&request_id=
        Tear down: Удалить аккаунт

    """

    with allure.step('Запрос changebalance на пополнение 100.000 аккауниу 101'):
        changebalance_1 = wik4.request(wik_mt4_addr,'changebalance', login=101, value=100000, comment='test_changebalance')
        print('Ответ Шаг1:\n', changebalance_1)
        assert '1' == changebalance_1['result'] and float(changebalance_1['newbalance']) == float(100000)

    with allure.step('Запрос changebalance с опц. параметром freemarginlimit, снятие невозможно'):
        changebalance_2 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='-99000.01',
                                       comment='test_changebalance', freemarginlimit=90)
        print('Ответ Шаг2:\n', changebalance_2)
        assert '-3' == changebalance_2['result']

    with allure.step('Запрос changebalance с опц. параметром freemarginlimit, снятие возможно'):
        changebalance_3 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='-90000',
                                       comment='test_changebalance', freemarginlimit=90)
        print('Ответ Шаг3:\n', changebalance_3)
        assert '1' == changebalance_3['result'] and float(changebalance_3['newbalance']) == float(10000)
