import web_api_mt4 as wik4
import pytest
import time
import allure
import random
from config import wik_mt4_addr, wik_mt4_addr_secret



@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        time.sleep(0.1)


@allure.title("Запрос changecredit: Попытка списать в минус.")
@allure.severity('critical')
def test_changecredit_1(accounts):
    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changecredit на списание с аккаунта 10.000.
            Шаг2: Запросить getmargininfoex кредит аккаунта.
            Шаг3: Проверить ответ.
            Ожидаемый ответ:
            Шаг 1: result=-4&reason=after operation credit will be negative&login=101&request_id=
            Шаг 2: result=1&users=1&unsuccessfully_requested_users=&balance=0.00&credit=0.00&equity=0.00&margin=0.00&freeMargin=0.00&size=28
        Tear down: удалить аккаунт
    """
    with allure.step('Списать кредит 101 аккаунта в минус'):
        changecredit_minus = wik4.request(wik_mt4_addr, 'changecredit', login=101, value='-10000', comment='test_changecredit')
        print('Ответ Шаг1:\n', changecredit_minus)
        time.sleep(1)
    with allure.step('Проверка списания кредита 101 аккаунта'):
        getmargininfoex_0 = wik4.request(wik_mt4_addr, 'getmargininfoex', login=101)
        print('Ответ Шаг2:\n', getmargininfoex_0)
        with allure.step('Проверка ответа'):
            assert float(0) == float(getmargininfoex_0['credit']) and '-4' == changecredit_minus['result']


@allure.title("Запрос changecredit: Начисление и списание кредита.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_changecredit_2(accounts):

    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changecredit на пополнение аккаунта.
            Шаг2: Запросить getmargininfoex кредит аккаунта.
            Шаг3: Проверить ответ.
            Шаг4: Отправить запрос changecredit на списание с аккаунта.
            Шаг5: Запросить getmargininfoex кредит аккаунта.
            Шаг6: Проверить ответ.
        Tear down: Удалить аккаунт.
    """

    with allure.step('Пополнить кредит 101 аккаунта запросом changecredit.'):
        changecredit_1 = wik4.request(wik_mt4_addr, 'changecredit', login=101, value=10000, comment='test_changecredit')
        print('Ответ Шаг1:\n', changecredit_1)
        time.sleep(.100)
        with allure.step('Проверить зачисление средств на кредит'):
            getmargininfoex_0 = wik4.request(wik_mt4_addr, 'getmargininfoex', login=101)
            print('Ответ Шаг2:\n', getmargininfoex_0)
            with allure.step('Проверка ответов.'):
                assert float(10000) == float(getmargininfoex_0['credit'])
    with allure.step('Списание кредита запросом changecredit.'):
        changecredit_2 = wik4.request(wik_mt4_addr, 'changecredit', login=101, value="-555.55", comment='test_changecredit')
        print('Ответ Шаг4:\n', changecredit_2)
        time.sleep(.100)
        with allure.step('Проверка списания средств с кредита'):
            getmargininfoex_1 = wik4.request(wik_mt4_addr, 'getmargininfoex', login=101)
            print('Ответ Шаг5:\n', getmargininfoex_1)
            with allure.step('Проверка ответов.'):
                assert float(10000-555.55) == float(getmargininfoex_1['credit'])



@allure.title("Запрос changecredit: несуществующий аккаунт.")
@allure.severity('minor')
def test_changecredit_3():
    """
    Описание шагов:
        Тест:
            Шаг1: Отправить запрос changecredit на пополнение несуществующего аккаунта
            Шаг2: Проверить ответ.
            Ожидаемый ответ: result=-4&reason=no login(s) available&request_id=
    """
    reason ="no login(s) available"
    with allure.step('Запрос changecredit на пополнение несуществующего аккаунта'):
        changecredit_1 = wik4.request(wik_mt4_addr, 'changecredit', login=101, value='10000',
                                                   comment='test_changecredit')
        print('Ответ Шаг1:\n', changecredit_1)
        assert '-4' == changecredit_1['result'] and reason == changecredit_1['reason']


@allure.title("Запрос changecredit: пустой комментарий.")
@allure.severity('normal')
def test_changecredit_4(accounts):
    """
    Описание шагов:
        Setup: Создать аккаунт с логином 101.
        Тест:
            Шаг1: Отправить запрос changecredit с пустым комментарием.
            Шаг2: Проверить ответ.
            Ожидаемый ответ: result=-3&reason=invalid params&login=101&request_id=
        Tear down: Удалить аккаунт
    """
    reason ='invalid params'
    with allure.step('Запрос changecredit с пустым комментарием.'):
        changecredit_1 = wik4.request(wik_mt4_addr, 'changecredit', login=101, value=10000, comment='')
        print('Ответ Шаг1:\n', changecredit_1)
        with allure.step('Проверка ответа'):
            assert '-3' == changecredit_1['result'] and reason == changecredit_1['reason']