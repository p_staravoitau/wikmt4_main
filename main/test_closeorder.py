import web_api_mt4 as wik4
import pytest
import time
import allure
import random
from config import wik_mt4_addr, wik_mt4_addr_secret



@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    with allure.step('Начисление баланса на аккаунт'):
        wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000', comment='test_closeorder')
        time.sleep(.100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        time.sleep(0.1)


@allure.title("Запрос closeorder: закрытие одного открытого ордера")
@allure.severity('critical')
def test_closeorder_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
               - Начислить баланс на аккаунт
        Тест:
            Шаг1: Отправить запрос addorder на открыте ордера
            Шаг2: Отправить запрос closeorder на закрытие ордера
            Ожидаемый ответ:
            Шаг1: result=1&order=589575&request_id=
            Шаг2: result=1&size=0
        Tear down: удалить аккаунт
    """
    random_volume = random.randint(1, 10)
    with allure.step('Запрос addorder на открыте ордера на 101 аккаунте'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume)
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result']
    with allure.step('Запрос closeorder на закрытие раннее открытого ордера'):
        addorder_2 = wik4.request(wik_mt4_addr, 'closeorder', order=addorder_1['order'])
        print('Ответ Шаг2:\n', addorder_2)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_2['result']

@allure.title("Запрос closeorder: закрытие нескольких открытых ордеров")
@allure.severity('critical')
def test_closeorder_2(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
               - Начислить баланс на аккаунт
        Тест:
            Шаг1: Отправить запрос addorder на открыте двух ордеров
            Шаг2: Отправить запрос closeorder на закрытие двух ордеров
        Tear down: удалить аккаунт
    """
    random_volume = random.randint(1, 10)
    with allure.step('Запрос addorder на открыте двух ордеров на 101 аккаунте'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume)
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume)
        print('Ответ Шаг1:\n', addorder_1, addorder_2)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result']  and '1' == addorder_2['result']
    with allure.step('Запрос closeorder на закрытие раннее открытых ордеров'):
        addorder_3 = wik4.request(wik_mt4_addr, 'closeorder', order=addorder_1['order'] + ';' + addorder_2['order'])
        print('Ответ Шаг2:\n', addorder_3)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_3['result']


@allure.title("Запрос closeorder: закрытие нескольких открытых ордеров с указанной ценой")
@allure.severity('critical')
def test_closeorder_3(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
               - Начислить баланс на аккаунт
        Тест:
            Шаг1: Отправить запрос addorder на открыте двух ордеров
            Шаг2: Отправить запрос closeorder на закрытие двух ордеров c ценой 1 и 2 соответственно
            Шаг3: Отправить запрос getorder на проверку, что ордера закрылись с указаной ценой
        Tear down: удалить аккаунт
    """
    random_volume = random.randint(1, 10)
    with allure.step('Запрос addorder на открыте двух ордеров на 101 аккаунте'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume)
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume)
        print('Ответ Шаг1:\n', addorder_1, addorder_2)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result']  and '1' == addorder_2['result']
    with allure.step('Запрос closeorder на закрытие раннее открытых ордеров с ценой 1 и 2 соответственно'):
        addorder_3 = wik4.request(wik_mt4_addr, 'closeorder', order=addorder_1['order'] + ',1' + ';' + addorder_2['order'] + ',2')
        print('Ответ Шаг2:\n', addorder_3)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_3['result']
    with allure.step('Запрос getorder на проверку, что ордера закрылись с указаной ценой'):
        getorder_1 = wik4.request(wik_mt4_addr, 'getorder',
                                  order=addorder_1['order'] + ';' + addorder_2['order'] )
        print('Ответ Шаг3:\n', getorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == getorder_1['result'] and float(getorder_1["answer"][0]['close_price']) == float(1) and float(getorder_1["answer"][1]['close_price']) == float(2)
