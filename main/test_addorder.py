import web_api_mt4 as wik4
import pytest
import time
import allure
import random
import random_value as rand
from pytest_expect import expect
from config import wik_mt4_addr, wik_mt4_addr_secret



@pytest.fixture()
def create_account():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100,
                     password='123456Qw', password_investor='123456Qw')
        with allure.step('Начисление баланса на аккаунт'):
            wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000', comment='test_addorder')
    time.sleep(0.01)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        deleteaccount = wik4.request(wik_mt4_addr, 'deleteaccount', login=101)



@allure.title("Запрос addorder: открытие ордера с обязательными параметрами")
@allure.severity('blocker')
@pytest.mark.smoke
def test_addorder_1(create_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт 101
             - пополняем ему баланс на 100.000
        Тест:
            Шаг1: открываем на 101 аккаунте позицию c обязательными параметрами: login, symbol, volume
            (если параметр cmd не указан, открывается 'buy' ордер)
            Шаг2: запросом getopenedtradesforlogin запрашиваем информацию по позиции и сверяем с переданными данными
        Tear down: Удаляем аккаунт.
    """
    random_volume = rand.random_volume()
    with allure.step('Запрос addorder c обязательными параметрами: login, symbol, volume'):
        addorder = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume)
        with allure.step('Проверка ответа'):
            assert '1' == addorder['result'] and "" != addorder['order']
        with allure.step('Запрос getopenedtradesforlogin'):
            getopenedtradesforlogin = wik4.request(wik_mt4_addr, 'getopenedtradesforlogin', login=101)
            with allure.step('Проверка изменения параметров'):
                print(getopenedtradesforlogin)
                with allure.step('Проверка, что cmd = 0'):
                    assert 'buy' == str(getopenedtradesforlogin["answer"][0]['action'])
                with allure.step('Проверка, что значение sl = 0'):
                    assert str(float(0)) == str(float(getopenedtradesforlogin["answer"][0]['sl']))
                with allure.step('Проверка, что значение tp = 0'):
                    assert str(float(0)) == str(float(getopenedtradesforlogin["answer"][0]['tp']))
                with allure.step('Проверка, что значение volume совпадает с отправленным'):
                    assert str(random_volume) == str(getopenedtradesforlogin["answer"][0]['volume'])

    
@allure.title("Запрос addorder: проверка работы запроса c необязательными параметрами (cmd, sl, tp, open_price, "
              "request_id, open_time)")
@allure.severity('normal')
@pytest.mark.smoke
def test_addorder_2(create_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт 101
             - пополняем баланс на 100.000
        Тест:
            Шаг1: вбрасываем котировку
            Шаг2: открываем на 101 аккаунте позицию c необязательными параметрами
            (cmd, sl, tp, open_price, request_id, open_time)
            Шаг3: запросом getopenedtradesforlogin запрашиваем информацию по позиции и сверяем с переданными данными
        Tear down: Удаляем аккаунт.
    """
    with allure.step('Генерируем случайные числа для параметров sl, tp, volume и price'):

        random_volume = rand.random_volume()
        random_price = round(random.uniform(0.90000, 1.00009), 5)
        random_tp_for_sell = round(random.uniform(0.0010, 0.09000), 3)
        random_sl_for_sell = round(random.uniform(1.01, 1.09), 3)
        open_time = int(time.time())

    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.00000, ask=1.00008)
        time.sleep(2)
    with allure.step('Запрос addorder'):
        addorder = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume, cmd=1,
                                sl=random_sl_for_sell, tp=random_tp_for_sell, open_price=random_price,
                                request_id=123, open_time=open_time)
        with allure.step('Проверка ответа'):
            assert '1' == addorder['result'] and "" != addorder['order']
        with allure.step('Запрос getopenedtradesforlogin'):
            getopenedtradesforlogin = wik4.request(wik_mt4_addr, 'getopenedtradesforlogin', login=101)
            with allure.step('Проверка изменения параметров'):
                print(getopenedtradesforlogin)
                with allure.step('Проверка значения sl'):
                    assert str(random_sl_for_sell) == str(float(getopenedtradesforlogin["answer"][0]['sl']))
                with allure.step('Проверка значения tp'):
                    assert str(random_tp_for_sell) == str(float(getopenedtradesforlogin["answer"][0]['tp']))
                with allure.step('Проверка времени открытия'):
                    assert str(open_time) == str(getopenedtradesforlogin["answer"][0]['open_time'])
                with allure.step('Проверка цены открытия'):
                    assert str(random_price) == str(float(getopenedtradesforlogin["answer"][0]['open_price']))
                with allure.step('Проверка, что cmd = 1'):
                    assert 'sell' == str(getopenedtradesforlogin["answer"][0]['action'])
                with allure.step('Проверка, что значение volume совпадает с отправленным'):
                    assert str(random_volume) == str(getopenedtradesforlogin["answer"][0]['volume'])


@allure.title("Запрос addorder: некорректные cmd (cmd>5).")
@allure.severity('normal')
def test_addorder_3(create_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт 101
             - пополняем баланс
        Тест:
            Шаг1: отправляем запрос addorder с cmd = 6
            Шаг2: отправляем запрос addorder с cmd = 7
            Ожидаемый ответ: result=-3&reason=invalid incoming parameters&request_id=
        Tear down: Удаляем аккаунт.
    """
    reason = "invalid incoming parameters"

    with allure.step('Запрос addorder с cmd = 6'):
        addorder_cmd6 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=6, volume=100)
        print('Ответ Шаг1:\n', addorder_cmd6)
    with allure.step('Запрос addorder с cmd = 7'):
        addorder_cmd7 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=7, volume=100)
        print('Ответ Шаг2:\n', addorder_cmd7)
    with allure.step('Проверка ответа'):
        assert '-3' == addorder_cmd6['result'] and reason == addorder_cmd6['reason']
        assert '-3' == addorder_cmd7['result'] and reason == addorder_cmd7['reason']


@allure.title("Запрос addorder: несуществующий логин")
@allure.severity('minor')
def test_addorder_4():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг0: вбрасываем котировку
            Шаг1: отправляем запрос addorder с несуществующим логином
            Ожидаемый ответ: result=-4&logins_notfound=102&request_id=
        Tear down: -
    """

    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.0000, ask=1.00008)
        time.sleep(1)
    with allure.step('Запрос addorder с несуществующим логином'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=102, symbol="EURUSD", cmd=0, volume=100)
        print('Ответ Шаг1:\n', addorder_1)
    with allure.step('Проверка ответа'):
        assert '-4' == addorder_1['result'] and '102' == addorder_1['logins_notfound']


@allure.title("Запрос addorder: несуществующий символ")
@allure.severity('minor')
def test_addorder_5(create_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт 101
             - пополняем баланс
        Тест:
            Шаг0: вбрасываем котировку
            Шаг1: отправляем запрос addorder с несуществующим символом
            Ожидаемый ответ: result=-4&symbols_notfound=EEEEEURUSD&request_id=
        Tear down: Удаляем аккаунт
    """

    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.0000, ask=1.00008)
        time.sleep(1)
    with allure.step('Запрос addorder с несуществующим символом'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EEEEEURUSD", cmd=0, volume=100)
        print('Ответ Шаг1:\n', addorder_1)
    with allure.step('Проверка ответа'):
        assert '-4' == addorder_1['result'] and "EEEEEURUSD" == addorder_1['symbols_notfound']


@allure.title("Запрос addorder: некорректные значения в обязательных параметрах.")
@allure.severity('minor')
def test_addorder_6(create_account):
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: отправить запрос addorder с некорректным значением в login
            Шаг2: отправить запрос addorder с некорректным значением в cmd
            Шаг3: отправить запрос addorder с некорректным значением в volume
            Ожидаемый ответ: result=-3&reason=Invalid parameters&request_id=
        Tear down: -
    """
    reason = "Invalid parameters"
    with allure.step('Запрос addorder с некорректным значением в login'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login="asd", symbol="EURUSD", cmd=0, volume=100)
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert ('-3' == addorder_1['result']) and (reason == addorder_1['reason'])
    with allure.step('Запрос addorder с некорректным значением в cmd'):
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd="asd", volume=100)
        print('Ответ Шаг2:\n', addorder_2)
        with allure.step('Проверка ответа'):
            assert('-3' == addorder_2['result'] and reason == addorder_2['reason'])
    with allure.step('Запрос addorder с некорректным значением в volume'):
        addorder_3 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume="asd")
        print('Ответ Шаг3:\n', addorder_3)
        with allure.step('Проверка ответа'):   
            assert('-3' == addorder_3['result'] and reason == addorder_3['reason'])



@allure.title("Запрос addorder: на аккаунте недостаточно средств")
@allure.severity('minor')
def test_addorder_7(create_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт 101
             - пополняем баланс
        Тест:
            Шаг1: списываем баланс 101 аккаунта
            Шаг2: отправляем запрос addorder
            Шаг3: проверяем ответ
            Ожидаемый ответ: result=-3&reason=Failed to open order&request_id=
        Tear down: -
    """
    reason = "Failed to open order"
    with allure.step('Списание баланса аккаунта'):
        wik4.request(wik_mt4_addr, 'changebalance', login=101, value='-100000', comment='test_addorder')
    with allure.step('Запрос addorder '):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=100)
        print('Ответ Шаг2:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_1['result'] and reason == addorder_1['reason']


@allure.title("Запрос addorder: некорректные обязательные параметры или их отсутствие.")
@allure.severity('minor')
def test_addorder_8():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: отправляем запрос addorder с ошибкой в параметре login
            Шаг2: отправляем запрос addorder с ошибкой в параметре volume
            Шаг3: отправляем запрос addorder с ошибкой в параметре symbol
            Шаг4: отправляем запрос addorder без параметра login
            Шаг5: отправляем запрос addorder без параметра volume
            Шаг6: отправляем запрос addorder без параметра symbol
            Ожидаемый ответ: reason=invalid params (required params are (UInt64)login, (String)symbol, (UInt32)cmd, (Decimal)volume)&result=-3&size=113
        Tear down: -
    """
    reason = "Invalid parameters"
    with allure.step('Запрос addorder с ошибкой в параметре login'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', lon=101, symbol="EURUSD", volume=100)
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_1['result'] and reason == addorder_1['reason']
    with allure.step('Запрос addorder с ошибкой в параметре volume'):
        addorder_3 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volum=100)
        print('Ответ Шаг2:\n', addorder_3)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_3['result'] and reason == addorder_3['reason']
    with allure.step('Запрос addorder с ошибкой в параметре symbol'):
        addorder_4 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbl="EURUSD", volume=100)
        print('Ответ Шаг3:\n', addorder_4)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_4['result'] and reason == addorder_4['reason']
    with allure.step('Запрос addorder без параметра login'):
        addorder_5 = wik4.request(wik_mt4_addr, 'addorder',  symbol="EURUSD", volume=100)
        print('Ответ Шаг4:\n', addorder_5)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_5['result'] and reason == addorder_5['reason']
    with allure.step('Запрос addorder без параметра volume'):
        addorder_7 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD",)
        print('Ответ Шаг5:\n', addorder_7)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_7['result'] and reason == addorder_7['reason']
    with allure.step('Запрос addorder без параметра symbol'):
        addorder_8 = wik4.request(wik_mt4_addr, 'addorder', login=101, volume=100)
        print('Ответ Шаг6:\n', addorder_8)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_8['result'] and reason == addorder_8['reason']


@allure.title("Запрос addorder: опциональные параметры price/sl/tp.")
@allure.severity('normal')
def test_addorder_9(create_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт 101
             - пополняем баланс
        Тест:
            Шаг1: отправляем запрос addorder sl>price при cmd=0
            Шаг2: отправляем запрос addorder sl<price при cmd=1
            Шаг3: отправляем запрос addorder sl=price
            Шаг4: отправляем запрос addorder tp>price при cmd=1
            Шаг5: отправляем запрос addorder tp<price при cmd=0
            Шаг6: отправляем запрос addorder tp=price
            Шаг7: отправляем запрос addorder sl=price=tp
            Ожидаемый ответ: result=-3&reason=Failed to open order&orderid=0&request_id=
        Tear down: -
    """
    reason_1 = "Failed to open order"
    with allure.step('Запрос addorder с sl>open_price при cmd=0'):
        wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.019, ask=1.02)
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=100, sl=2.000, open_price=1.0)
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_1['result'] and reason_1 == addorder_1['reason']
    with allure.step('Запрос addorder с sl<open_price при cmd=1'):
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=1, volume=100, sl=0.500, open_price=1.0)
        print('Ответ Шаг2:\n', addorder_2)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_2['result'] and reason_1 == addorder_2['reason']
    with allure.step('Запрос addorder с sl=open_price'):
        wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.00, ask=1.01)
        addorder_3 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=100, sl=1.000, open_price=1.0)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_3['result'] and reason_1 == addorder_3['reason']
    with allure.step('Запрос addorder с tp>open_price при cmd=1'):
        addorder_4 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=1, volume=1, tp=2.000, open_price=1.0)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_4['result'] and reason_1 == addorder_4['reason']
    with allure.step('Запрос addorder с tp<price при cmd=0'):
        addorder_5 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=1, tp=0.500, open_price=1.0)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_5['result'] and reason_1 == addorder_5['reason']
    with allure.step('Запрос addorder с tp=price=sl'):
        addorder_7 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=1, sl=1.000, tp=1.000, open_price=1.0)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_7['result'] and reason_1 == addorder_7['reason']


@allure.title("Запрос addorder: некорректные значения в опциональных параметрах.")
@allure.severity('trivial')
def test_addorder_10(create_account):
    """
    Описание шагов:
        Setup: создать аккаунт 101
             - пополнить баланс
        Тест:
            Шаг1: отправить запрос addorder с некорректным значением sl
            Шаг2: отправить запрос addorder с некорректным значением price
            Шаг3: отправить запрос addorder с некорректным значением tp
            Ожидаемый ответ: result=-3&reason=parsing_problem&request_id=
        Tear down: Удалить аккаунт.
    """
    reason_1 = "Failed to open order"
    with allure.step('Запрос addorder с некорректным значением sl'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=100, sl="asd", open_price=1.0)
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_1['result'] and reason_1 == addorder_1['reason']
    with allure.step('Запрос addorder с некорректным значением price'):
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=1, volume=100, sl=0.500, open_price="asd")
        print('Ответ Шаг2:\n', addorder_2)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_2['result'] and reason_1 == addorder_2['reason']
    with allure.step('Запрос addorder с некорректным значением tp'):
        addorder_3 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", cmd=0, volume=100, tp="asd", open_price=1.0)
        print('Ответ Шаг3:\n', addorder_3)
        with allure.step('Проверка ответа'):
            assert '-3' == addorder_3['result'] and reason_1 == addorder_3['reason']
