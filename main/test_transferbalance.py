import web_api_mt4 as wik4
import pytest
import allure
import time
import random_value as rand
from config import wik_mt4_addr, wik_mt4_addr_secret

@pytest.fixture()
def accounts():
    for login in range(101, 104):
        with allure.step('Создать аккаунты'):
            wik4.request(wik_mt4_addr, 'createaccount', login=login, name='QA', group='demoforex', leverage=100)
        with allure.step('Начислить баланс на аккаунты'):
            wik4.request(wik_mt4_addr, 'changebalance', login=login, value='10000', comment='test_transferbalance')
    time.sleep(.100)
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        for login in range(101, 104):
            wik4.request(wik_mt4_addr, 'deleteaccount', login=login)


@allure.title("Запрос transferbalance: один from аккаунт, один receiver аккаунт")
@allure.severity('blocker')
@pytest.mark.smoke
def test_transferbalance_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунты 101, 102, 103
               - Начислить 10000 баланса на все аккаунты
        Тест:
            Шаг1: Отправить запрос transferbalance: один from аккаунт, один receiver аккаунт
                  Ожидаемый ответ: result=1&size=82
                                   101;10213.92;0.00;10213.92;0.00;10213.92;1
                                   102;9786.08;0.00;9786.08;0.00;9786.08;1&request_id=
            Шаг2: Получить время сервера МТ4
            Шаг3: Запрос getbalancesoperations: получение информации о балансовых операциях
        Tear down: Удаление аккаунтов
    """
    random_amount = rand.random_amount(100, 1000)
    with allure.step('Запрос transferbalance: один from аккаунт, один receiver аккаунт'):
        transferbalance_1 = wik4.request(wik_mt4_addr, 'transferbalance', receiver_logins=101,
                                         receiver_comment='transferbalance_add', from_logins=102,
                                         from_comment='transferbalance_del', amount=random_amount)
        print('Ответ Шаг1:\n', transferbalance_1)
        assert transferbalance_1['result'] == '1' and transferbalance_1['answer'][0]['login'] == '101' \
               and float(transferbalance_1['answer'][0]['equity']) == (float(10000) + random_amount) \
               and float(transferbalance_1['answer'][0]['margin_free']) == (float(10000) + random_amount) \
               and transferbalance_1['answer'][0]['success_flag'] == '1' \
               and float(transferbalance_1['answer'][0]['balance']) == (float(10000) + random_amount) \
               and transferbalance_1['answer'][1]['login'] == '102'    \
               and float(transferbalance_1['answer'][1]['balance']) == (float(10000) - random_amount) \
               and float(transferbalance_1['answer'][1]['equity']) == (float(10000) - random_amount) \
               and float(transferbalance_1['answer'][1]['margin_free']) == (float(10000) - random_amount) \
               and transferbalance_1['answer'][1]['success_flag'] == '1'
    with allure.step('Запрос servertime'):
        servertime_1 = wik4.request(wik_mt4_addr, 'servertime')
        print('Ответ Шаг2:\n', servertime_1)
        with allure.step('Проверка ответа'):
            assert '1' == servertime_1['result']
    with allure.step('Запрос getbalancesoperations: получение информации о балансовых операциях'):
        getbalancesoperations_1 = wik4.request(wik_mt4_addr, 'getbalancesoperations', **{'login':'101',
                                                'from': int(servertime_1['time']) - 100, 'to': int(servertime_1['time']) + 100})
        getbalancesoperations_2 = wik4.request(wik_mt4_addr, 'getbalancesoperations', **{'login': '102',
                                                'from': int(servertime_1['time']) - 100, 'to': int(servertime_1['time']) + 100})
        print('Ответ Шаг3:\n', getbalancesoperations_1, getbalancesoperations_2)
        with allure.step('Проверка ответа'):
            assert '1' == getbalancesoperations_1['result'] \
                   and getbalancesoperations_1['answer'][1]['comment'] == 'transferbalance_add' \
                   and float(getbalancesoperations_1['answer'][1]['profit']) == random_amount
            assert '1' == getbalancesoperations_2['result'] \
                   and getbalancesoperations_2['answer'][1]['comment'] == 'transferbalance_del' \
                   and float(getbalancesoperations_2['answer'][1]['profit']) == -random_amount