import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунты'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
        wik4.request(wik_mt4_addr, 'createaccount', login=102, name='QA', group='demoforex', leverage=100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        wik4.request(wik_mt4_addr, 'deleteaccount', login=102)
        time.sleep(0.1)



@allure.title("Запрос bulkmodifyaccount: проверка работы - один аккаунт.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_bulkmodifyaccount_1(accounts):
    """
    Описание шагов:
        Setup: - создать аккаунты с логинами 101 и 102.
        Тест:
            Шаг1: отправить запрос bulkmodifyaccount на 101 аккаунт со всеми доп параметрами.
            Шаг2: запросить данные аккаунта запросом getaccountinfoex
            Шаг3: сделать проверку измененных параметров
        Tear down: удаление аккаунтов
    """
    with allure.step('Запрос modifyaccount'):
        wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login=101, name='Test', group='demoforex', agent=102, leverage=1000,
                     email='email_Test', id="id", address='address_Test', city='city_Test', state='state_Test', zipcode='zipcode_Test',
                     country='country_Test', phone='phone_Test123', password_phone='123456Qw', comment='comment_Test', enable=1,
                     enable_change_password=1, enable_read_only=0, send_reports=1, status='status_Test')
    with allure.step('Проверка измененных параметров'):
        getaccountinfoex_101 = wik4.request(wik_mt4_addr, 'getaccountinfoex', login=101)
        with allure.step('Проверка измененния параметра leverage'):
            assert '1000' == getaccountinfoex_101['leverage']
        with allure.step('Проверка измененния параметра agent'):
            assert '102' == getaccountinfoex_101['agent']
        with allure.step('Проверка измененния параметра address'):
            assert 'address_Test' == getaccountinfoex_101['address']
        with allure.step('Проверка измененния параметра name'):
            assert 'Test' == getaccountinfoex_101['name']
        with allure.step('Проверка измененния параметра email'):
            assert 'email_Test' == getaccountinfoex_101['email']
        with allure.step('Проверка измененния параметра status'):
            assert 'status_Test' == getaccountinfoex_101['status']
        with allure.step('Проверка измененния параметра id'):
            assert 'id' == getaccountinfoex_101['id']
        with allure.step('Проверка измененния параметра city'):
            assert 'city_Test' == getaccountinfoex_101['city']
        with allure.step('Проверка измененния параметра state'):
            assert 'state_Test' == getaccountinfoex_101['state']
        with allure.step('Проверка измененния параметра zipcode'):
            assert 'zipcode_Test' == getaccountinfoex_101['zip']
        with allure.step('Проверка измененния параметра country'):
            assert 'country_Test' == getaccountinfoex_101['country']
        with allure.step('Проверка измененния параметра phone'):
            assert 'phone_Test123' == getaccountinfoex_101['phone']
        with allure.step('Проверка измененния параметра comment'):
            assert 'comment_Test'== getaccountinfoex_101['comment']
        with allure.step('Проверка измененния параметра send_reports'):
            assert '1' == getaccountinfoex_101['send_reports']
        with allure.step('Проверка измененния параметра enable'):
            assert '1' == getaccountinfoex_101['enable']
        with allure.step('Проверка измененния параметра enable_change_password'):
            assert '1' == getaccountinfoex_101['enableChangePassword']


@allure.title("Запрос bulkmodifyaccount: проверка работы - перечень аккаунтов.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_bulkmodifyaccount_2(accounts):
    """
       Описание шагов:
        Setup: - создать аккаунты с логинами 101 и 102.
        Тест:
            Шаг1: отправить запрос bulkmodifyaccount на 101,102 аккаунты со всеми доп параметрами.
            Шаг2: запросить данные аккаунта запросом getaccountinfoex
            Шаг3: сделать проверку измененных параметров
        Tear down: удаление аккаунтов
    """

    with allure.step('Запрос bulkmodifyaccount'):
        wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101;102', name='Test', group='demoforex', agent=1000, leverage=1000,
                     email='email_Test', id="id", address='address_Test', city='city_Test', state='state_Test', zipcode='zipcode_Test',
                     country='country_Test', phone='phone_Test123', password_phone='123456Qw', comment='comment_Test', enable=1,
                     enable_change_password=1, enable_read_only=0, send_reports=1, status='status_Test')
    with allure.step('Проверка измененных параметров'):
        getaccountinfoex_101 = wik4.request(wik_mt4_addr, 'getaccountinfoex', login=101)
        getaccountinfoex_102 = wik4.request(wik_mt4_addr, 'getaccountinfoex', login=102)
        with allure.step('Проверка измененния параметра leverage'):
            assert '1000' == getaccountinfoex_101['leverage'] and '1000' == getaccountinfoex_102['leverage']
        with allure.step('Проверка измененния параметра agent'):
            assert '1000' == getaccountinfoex_101['agent'] and '1000' == getaccountinfoex_102['agent']
        with allure.step('Проверка измененния параметра address'):
            assert 'address_Test' == getaccountinfoex_101['address'] and 'address_Test' == getaccountinfoex_102['address']
        with allure.step('Проверка измененния параметра name'):
            assert 'Test' == getaccountinfoex_101['name'] and 'Test' == getaccountinfoex_102['name']
        with allure.step('Проверка измененния параметра email'):
            assert 'email_Test' == getaccountinfoex_101['email'] and 'email_Test' == getaccountinfoex_102['email']
        with allure.step('Проверка измененния параметра status'):
            assert 'status_Test' == getaccountinfoex_101['status'] and 'status_Test' == getaccountinfoex_102['status']
        with allure.step('Проверка измененния параметра id'):
            assert 'id' == getaccountinfoex_101['id'] and 'id' == getaccountinfoex_102['id']
        with allure.step('Проверка измененния параметра city'):
            assert 'city_Test' == getaccountinfoex_101['city'] and'city_Test' == getaccountinfoex_102['city']
        with allure.step('Проверка измененния параметра state'):
            assert 'state_Test' == getaccountinfoex_101['state'] and 'state_Test' == getaccountinfoex_102['state']
        with allure.step('Проверка измененния параметра zipcode'):
            assert 'zipcode_Test' == getaccountinfoex_101['zip'] and 'zipcode_Test' == getaccountinfoex_102['zip']
        with allure.step('Проверка измененния параметра country'):
            assert 'country_Test' == getaccountinfoex_101['country'] and 'country_Test' == getaccountinfoex_102['country']
        with allure.step('Проверка измененния параметра phone'):
            assert 'phone_Test123' == getaccountinfoex_101['phone'] and 'phone_Test123' == getaccountinfoex_102['phone']
        with allure.step('Проверка измененния параметра comment'):
            assert 'comment_Test' == getaccountinfoex_101['comment'] and 'comment_Test' == getaccountinfoex_102['comment']
        with allure.step('Проверка измененния параметра send_reports'):
            assert '1' == getaccountinfoex_101['send_reports'] and '1' == getaccountinfoex_102['send_reports']
        with allure.step('Проверка измененния параметра enable'):
            assert '1' == getaccountinfoex_101['enable'] and '1' == getaccountinfoex_102['enable']
        with allure.step('Проверка измененния параметра enable_change_password'):
            assert '1' == getaccountinfoex_101['enableChangePassword'] and '1' == getaccountinfoex_102['enableChangePassword']


@allure.title("Запрос bulkmodifyaccount: несуществующий аккаунт.")
@allure.severity('minor')
def test_bulkmodifyaccount_3():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: отправить запрос bulkmodifyaccount с несуществующим аккаунтом.
            Шаг2: проверить ответ
            Ожидаемый ответ:result=-4&reason=logins not found&size=3
            105&request_id=
    Tear down:-
   """
    reason = "logins not found"
    with allure.step('Запрос bulkmodifyaccount'):
        bulkmodifyaccount = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login=105, name='Test')
    with allure.step('Проверка ответа'):
        assert '-4' == bulkmodifyaccount['result'] and reason == bulkmodifyaccount['reason'] 


@allure.title("Запрос bulkmodifyaccount: в перечне несуществующий аккаунт.")
@allure.severity('critical')
def test_bulkmodifyaccount_4(accounts):
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: отправить запрос bulkmodifyaccount с несуществующим аккаунтом в перечне
            Шаг2: запросить данные аккаунта запросом getaccountinfo
            Шаг3: проверка изменения параметров на существующем аккаунте
            Шаг4: проверка ответа bulkmodifyaccount
            Ожидаемый ответ:result=1&size=3
                            105&request_id=
        Tear down:-
    """

    with allure.step('Запрос bulkmodifyaccount с несуществующим в перечне аккаунтом'):
        bulkmodifyaccount = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101;105', name='Test_1234')
        print('Ответ Шаг1:\n', bulkmodifyaccount)
    with allure.step('Проверка измененных параметров'):
        getaccountinfo_101 = wik4.request(wik_mt4_addr, 'getaccountinfo', login=101)
        print('Ответ Шаг2:\n', getaccountinfo_101)
        with allure.step('Проверка изменения параметров у существующего логина'):
            assert "Test_1234" == getaccountinfo_101['name']
        with allure.step('Проверка ответа bulkmodifyaccount'):
            assert '1' == bulkmodifyaccount['result'] and '105' == bulkmodifyaccount['answer'][0]['logins']


@allure.title("Запрос bulkmodifyaccount: некорректное значения в параметре 'login' (отличное от типа int)")
@allure.severity('minor')
def test_bulkmodifyaccount_5():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: оправить запрос bulkmodifyaccount с login='asd'
            Шаг2: проверить ответ
            Ожидаемый ответ: result=-3&reason=invalid params&size=3&request_id=
        Tear down: -
   """
    reason = 'invalid params'
    with allure.step('Запрос bulkmodifyaccount'):
        bulkmodifyaccount = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='asd', name='Test')
    with allure.step('Проверка ответа'):
        assert reason == bulkmodifyaccount['reason'] and '-3' == bulkmodifyaccount['result']


@allure.title("Запрос bulkmodifyaccount:  в перечне некоррекнтное значение.")
@allure.severity('critical')
def test_bulkmodifyaccount_6(accounts):
    """
    Описание шагов:
        Setup: создаём аккаунты с логином 101 и 102.
        Тест:
            Шаг1: отправляем запрос bulkmodifyaccount с login='101;asd'
            Шаг2: запрашиваем данные аккаунта запросом getaccountinfo
            Шаг3: проверка изменения параметров на существующий аккаунте
            Шаг4: проверка ответа bulkmodifyaccount
            Ожидаемый ответ: result=1&reason=&size=42&
                             asd&request_id=
            Шаг5: отправляем запрос bulkmodifyaccount с login='105;asd'
            Ожидаемый ответ: result=-4&reason=&size=42&
                             105
                             asd&request_id=
        Tear down: удаление аккаунтов
   """
    with allure.step('Запрос bulkmodifyaccount с одним некорректным в перечне'):
        bulkmodifyaccount = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101;asd', name='Test_1234')
        print('Ответ Шаг1:\n', bulkmodifyaccount)
        with allure.step('Проверка измененных параметров'):
            getaccountinfo_101 = wik4.request(wik_mt4_addr, 'getaccountinfo', login=101)
            print('Ответ Шаг2:\n', getaccountinfo_101)
            with allure.step('Проверка изменения параметров у существующего логина'):
                assert "Test_1234" == getaccountinfo_101['name']
            with allure.step('Проверка ответа'):
                assert '1' == bulkmodifyaccount['result'] and 'asd' == bulkmodifyaccount['answer'][0]['logins']
    with allure.step('Запрос bulkmodifyaccount с несуществующим и некорректным'):
        bulkmodifyaccount_1 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='105;asd', name='Test')
        print('Ответ Шаг5:\n', bulkmodifyaccount_1)
        with allure.step('Проверка ответа'):
            assert '-4' == bulkmodifyaccount_1['result'] and '105' == bulkmodifyaccount_1['answer'][0]['logins'] and 'asd' == bulkmodifyaccount_1['answer'][1]['logins']


@allure.title("Запрос bulkmodifyaccount: некорректные обязательные параметры или их отсутствие.")
@allure.severity('minor')
def test_bulkmodifyaccount_7(accounts):
    """
    Описание шагов:
        Setup: - создать аккаунты с логином 101 и 102.
        Тест:
            Шаг1: отправить запрос bulkmodifyaccount с ошибкой в параметре login
            Шаг2: отправить запрос bulkmodifyaccount без параметра login
            Ожидаемый ответ: "result=-3&reason=invalid params&size=69"
        Tear down: - удаление аккаунтов
   """
    reason = 'invalid params'
    with allure.step('Запрос bulkmodifyaccount с ошибкой в параметре login'):
        bulkmodifyaccount_1 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', lin='101')
    with allure.step('Запрос bulkmodifyaccount без параметра login'):
        bulkmodifyaccount_2 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount',)
    with allure.step('Проверка ответа'):
        assert reason == bulkmodifyaccount_1['reason'] and '-3' == bulkmodifyaccount_1['result']
        assert reason == bulkmodifyaccount_2['reason'] and '-3' == bulkmodifyaccount_2['result']


@allure.title("Запрос bulkmodifyaccount:  некорректные значения в опциональных параметрах.")
@allure.severity('trivial')
def test_bulkmodifyaccount_8(accounts):
    """
    Описание шагов:
        Setup: - создать аккаунты с логином 101 и 102.
        Тест:
            Шаг1: отправить запрос bulkmodifyaccount с некоректными данными в agent_account
            Шаг2: отправить запрос bulkmodifyaccount с некоректными данными в leverage
            Шаг3*: отправить запрос bulkmodifyaccount с некоректными данными в group (несуществующая группа)
            Шаг4: отправить запрос bulkmodifyaccount с некоректными данными в enable
            Шаг5: отправить запрос bulkmodifyaccount с некоректными данными в enable_change_password
            Шаг6: отправить запрос bulkmodifyaccount с некоректными данными в enable_read_only
            Шаг7: отправить запрос bulkmodifyaccount с некоректными данными в enable_daily_reports

            Ожидаемый ответ: result=-3&reason=parsing_problem&request_id=
        Tear down: удаляем акаунты
    """
    reason = 'invalid params'
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в agent_account.'):
        bulkmodifyaccount_1 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',  agent='asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_1['reason'] and '-3' == bulkmodifyaccount_1['result']
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в leverage.'):
        bulkmodifyaccount_2 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',  leverage='asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_2['reason'] and '-3' == bulkmodifyaccount_2['result']
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в group.'):
        bulkmodifyaccount_3 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',  group='asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_3['reason'] and '-4' == bulkmodifyaccount_3['result']
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в enable.'):
        bulkmodifyaccount_4 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',  enable = 'asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_4['reason'] and '-3' == bulkmodifyaccount_4['result']
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в enable_change_password.'):
        bulkmodifyaccount_5 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',  enable_change_password = 'asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_5['reason'] and '-3' == bulkmodifyaccount_5['result']
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в enable_read_only.'):
        bulkmodifyaccount_6 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',  enable_read_only = 'asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_6['reason'] and '-3' == bulkmodifyaccount_6['result']
    with allure.step('Запрос bulkmodifyaccount с некоректными данными в enable_daily_reports.'):
        bulkmodifyaccount_7 = wik4.request(wik_mt4_addr, 'bulkmodifyaccount', login='101',   enable_daily_reports= 'asd')
        with allure.step('Проверка ответа'):
            assert reason == bulkmodifyaccount_7['reason'] and '-3' == bulkmodifyaccount_7['result']