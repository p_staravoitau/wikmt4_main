import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунты'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
        wik4.request(wik_mt4_addr, 'createaccount', login=102, name='QA', group='demoforex', leverage=100)
    time.sleep(0.1)
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        for login in range(101, 103):
            wik4.request(wik_mt4_addr, 'deleteaccount', login=login)



@allure.title("Запрос bulkchangebalance: Попытка списать в минус.")
@allure.severity('critical')
@pytest.mark.smoke
def test_bulkchangebalance_1(accounts):
    """
    Описание шагов:
        Setup: создаём аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangebalance на списание 10.000 с одного аккаунта
            Ожидаемый ответ: result=1&size=62
                            101;0.00;-1;0;"ERR: after operation balance will be negative"&request_id=
            Шаг2: отправить запрос bulkchangebalance на списание 10.000 с двух аккаунтов (101, 102)
            Ожидаемый ответ: result=1&size=123
                            101;0.00;-1;0;"ERR: after operation balance will be negative"
                            102;0.00;-1;0;"ERR: after operation balance will be negative"&request_id=
    Tear down: удаление аккаунтов
    """
    with allure.step('Списание баланса в минус - один аккаунт'):
        bulkchangebalance_minus_0 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins=101, value='-10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_minus_0['result'] and "0" == bulkchangebalance_minus_0['answer'][0]['success_flag']
    with allure.step('Списание баланса в минус - перечень аккаунтов'):
        bulkchangebalance_minus_1 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101;102', value='-10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_minus_1['result'] and "0" == bulkchangebalance_minus_1['answer'][0]['success_flag'] and "0" == bulkchangebalance_minus_1['answer'][1]['success_flag']


@allure.title("Запрос bulkchangebalance: Начисление и списание баланса - один аккаунт")
@allure.severity('blocker')
def test_bulkchangebalance_2(accounts):
    """
    Описание шагов:
    Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangebalance на пополнение 10.000 аккаунта 101
            Ожидаемый ответ: result=1&size=27
                            101;10000.00;585908;1;"OK"&request_id=
            Шаг2: отправить запрос bulkchangebalance на списание 10.000 c аккаунта 101
            Ожидаемый ответ: result=1&size=23
                            101;0.00;585909;1;"OK"&request_id=
    Tear down: удалить аккаунты
    """
    with allure.step('Запрос bulkchangebalance на пополнение аккаунта'):
        bulkchangebalance_1 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins=101, value='10000', comment='test_balance')
        print('Ответ Шаг1:\n', bulkchangebalance_1)
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_1['result'] and float(10000) == float(bulkchangebalance_1['answer'][0]['newbalance'])
    with allure.step('Запрос bulkchangebalance на списание c аккаунта'):
        bulkchangebalance_2 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins=101, value='-10000', comment='test_balance')
        print('Ответ Шаг2:\n', bulkchangebalance_2)
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_2['result'] and float(0) == float(bulkchangebalance_2['answer'][0]['newbalance'])


@allure.title("Запрос bulkchangebalance: Начисление и списание баланса - в перечене несколько аккаунтов")
@allure.severity('blocker')
def test_bulkchangebalance_3(accounts):
    """
    Описание шагов:
        Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangebalance на пополнение аккаунтов 101;102 на 10.000.
            Ожидаемый ответ: result=1&size=46
                            101;10000.00;585913;1;"OK"
                            102;10000.00;585914;1;"OK"&request_id=
            Шаг2: отправить запрос bulkchangebalance на списание 10.000 c аккаунтов 101;102.
            Ожидаемый ответ: result=1&size=46
                            101;0.00;585915;1;"OK"
                            102;0.00;585916;1;"OK"&request_id=
    Tear down: удаление аккаунтов
    """
    with allure.step('Запрос bulkchangebalance на пополнение аккаунтов'):
        bulkchangebalance_101 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101;102', value='10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_101['result'] and float(10000) == float(bulkchangebalance_101['answer'][0]['newbalance']) and float(10000) == float(bulkchangebalance_101['answer'][1]['newbalance'])
    with allure.step('Запрос bulkchangebalance на списание c аккаунта'):
        bulkchangebalance_101 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101;102', value='-10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_101['result'] and float(0) == float(bulkchangebalance_101['answer'][0]['newbalance']) and float(0) == float(bulkchangebalance_101['answer'][1]['newbalance'])


@allure.title("Запрос bulkchangebalance: несуществующий аккаунт")
@allure.severity('minor')
def test_bulkchangebalance_4():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: отправить запрос bulkchangebalance на пополнение несуществующего аккаунта
            Ожидаемый ответ: result=1&size=36
                            100;0.00;-1;0;"ERR: logins_notfound"&request_id=
    Tear down: -
    """
    reason = '"ERR: logins_notfound"'
    with allure.step('Запрос bulkchangebalance на пополнение несуществующего аккаунта'):
        bulkchangebalance_100 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins=100, value='10000',
                                             comment='test_balance')
        print('Ответ Шаг1:\n', bulkchangebalance_100)
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_100['result'] and "0" == bulkchangebalance_100['answer'][0]['success_flag'] \
                   and reason == bulkchangebalance_100 ['answer'][0]['reason']


@allure.title("Запрос bulkchangebalance: в перечне несуществующий аккаунт.")
@allure.severity('minor')
def test_bulkchangebalance_5(accounts):
    """
    Описание шагов:
        Setup: создать аккаунты с логинами 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangebalance на пополнение аккаунтов на 10.000.
            Ожидаемый ответ: result=1&size=60
                            101;10000.00;2412935;1;"OK"
                            100;0.00;-1;0;"ERR: logins_notfound"&request_id=
            Шаг2: отправить запрос bulkchangebalance в перечне только несуществующие логины
            Ожидаемый ответ: result=1&size=73
                            104;0.00;-1;0;"ERR: logins_notfound"
                            105;0.00;-1;0;"ERR: logins_notfound"&request_id=
    Tear down: удаление аккаунта
        """
    reason = '"ERR: logins_notfound"'
    with allure.step('Запрос bulkchangebalance на пополнение аккаунтов, в перечне один несущестаующий аккаунт'):
        bulkchangebalance_100 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='100;101', value='10000',
                                             comment='test_balance')
        print('Ответ Шаг1:\n', bulkchangebalance_100)
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_100['result'] and '1' == bulkchangebalance_100['answer'][0]['success_flag']\
                   and float(10000) == float(bulkchangebalance_100['answer'][0]['newbalance']) \
                   and '0' == bulkchangebalance_100['answer'][1]['success_flag'] \
                   and reason == bulkchangebalance_100 ['answer'][1]['reason']
    with allure.step('Запрос bulkchangebalance на пополнение аккаунтов, в перечне два несуществующих аккаунта'):
        bulkchangebalance_100 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='104;105', value='10000',
                                             comment='test_balance')
        print('Ответ Шаг2:\n', bulkchangebalance_100)
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangebalance_100['result'] and '0' == bulkchangebalance_100['answer'][0]['success_flag'] \
                and reason == bulkchangebalance_100 ['answer'][0]['reason'] \
                and '0' == bulkchangebalance_100['answer'][1]['success_flag'] \
                and reason == bulkchangebalance_100 ['answer'][1]['reason']


@allure.title("Запрос bulkchangebalance: в перечне некоррекнтное значение.")
@allure.severity('critical')
def test_bulkchangebalance_6(accounts):
    """
    Описание шагов:
    Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangebalance на пополнение аккаунта 10.000. В перечне некоррекнтное значение 'asd'
            Ожидаемый ответ: result=-3&reason=invalid params&request_id=
            Шаг2: отправить запрос bulkchangebalance в перечне только asd;99 - некорректное значение и не существующий аккаунт
            Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: удаление аккаунта
    """
    reason = "invalid params"

    with allure.step('Запрос bulkchangebalance с некорректным значением в перечне'):
        bulkchangebalance_100 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101;asd', value='10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '-3' == bulkchangebalance_100['result'] and reason == bulkchangebalance_100['reason'] 
    with allure.step('Запрос bulkchangebalance с некорректным значением и несуществующим аккаунтом в перечне'):
        bulkchangebalance_100 = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='asd;99', value='10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '-3' == bulkchangebalance_100['result'] and reason == bulkchangebalance_100['reason']


@allure.title("Запрос bulkchangebalance: некорректные значения в параметрах.")
@allure.severity('minor')
def test_bulkchangebalance_7():
    """
    Описание шагов:
        Setup: -
            Тест:
                Шаг1: отправить запрос bulkchangebalance на пополнение аккаунта на 10.000 с logins=asd.
                Шаг2: отправить запрос bulkchangebalance на пополнение аккаунта на 10.000 с value=asd.
                Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: -

    """

    reason = 'invalid params'

    with allure.step('Запрос bulkchangebalance с параметром logins=asd'):
        bulkchangebalance_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='asd', value=10000, comment='asd')
        assert '-3' == bulkchangebalance_login['result'] and reason == bulkchangebalance_login["reason"]
    with allure.step('Запрос bulkchangebalance с параметром value=asd'):
        bulkchangebalance_value = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value='asd', comment='asd')
        assert '-3' == bulkchangebalance_value['result'] and reason == bulkchangebalance_value['reason']


@allure.title("Запрос bulkchangebalance: пустой комментарий.")
@allure.severity('minor')
def test_bulkchangebalance_8(accounts):
    """
    Описание шагов:
        Setup: создаmь аккаунт с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangebalance на пополнение аккаунта на 10.000 с comment=''.
            Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: удаление аккаунтов
     """
    reason = 'invalid params'
    with allure.step('Запрос bulkchangebalance c пустым комментарием '):
        bulkchangebalance_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value=10000, comment='')
        assert '-3' == bulkchangebalance_login['result'] and reason == bulkchangebalance_login['reason']


@allure.title("Запрос bulkchangebalance: ошибка в обязательном параметры или его отсутствие.")
@allure.severity('minor')
def test_bulkchangebalance_9(accounts):
    """
    Описание шагов:
        Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: ошибка в параметре login
            Шаг2: ошибка в параметре value
            Шаг3: ошибка в параметре comment
            Шаг4: отсутствует параметр comment
            Шаг5: отсутствует параметр value
            Шаг6: отсутствует параметр login
        Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: удаление аккаунтов
    """
    reason = 'invalid params'
    with allure.step('Запрос с ошибкой в параметре login'):
        bulkchangebalance_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', logns='101', value=10000, comment='asd')
        assert '-3' == bulkchangebalance_login['result'] and reason == bulkchangebalance_login['reason']
    with allure.step('Запрос с ошибкой в параметре value'):
        bulkchangebalance_value = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', valueeee=10000, comment='asd')
        assert '-3' == bulkchangebalance_value['result'] and reason == bulkchangebalance_value['reason']
    with allure.step('Запрос с ошибкой в параметре comment'):
        bulkchangebalance_comment = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value=10000, comnt='asd')
        assert '-3' == bulkchangebalance_comment['result'] and reason == bulkchangebalance_comment['reason']
    with allure.step('Запрос без параметра comment'):
        bulkchangebalance_no_comment = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value=10000)
        assert '-3' == bulkchangebalance_no_comment['result'] and reason == bulkchangebalance_no_comment['reason']
    with allure.step('Запрос без параметра login'):
        bulkchangebalance_no_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', value=10000, comment='asd')
        assert '-3' == bulkchangebalance_no_login['result'] and reason == bulkchangebalance_no_login['reason']
    with allure.step('Запрос без параметра value'):
        bulkchangebalance_no_value = wik4.request(wik_mt4_addr, 'bulkchangebalance', login='101', comment='asd')
        assert '-3' == bulkchangebalance_no_value['result'] and reason == bulkchangebalance_no_value['reason']
