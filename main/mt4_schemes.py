mt4_schemes = {
    'bulkchangebalance': ('login', 'newbalance', 'ticket', 'success_flag', 'reason'),
    'bulkchangecredit': ('login', 'newcredit', 'ticket', 'success_flag', 'reason'),
    'bulkmodifyaccount': ('logins',),
    'createaccountinrange': ('login',),
    'createaccountsinrange': ('login',),
    'deleteorder': ('order_no_delete',),
    'deletependingorder': ('order_no_delete',),
    'getaccountbalance': ('login', 'balance',),
    'getaccounts': (
        'login', 'name', 'email', 'group', 'leverage', 'regdate', 'country', 'state', 'adress', 'city', 'zip',
        'enable', 'tradingblocked', 'balance', 'comment', 'enableChangePassword', 'agent', 'last_access_date',),
    'getallbalances': ('login', 'balance',),
    'getbalancesoperations': ('ticket', 'profit', 'open_time', 'comment',),
    'getcharthistory': ('chart_ticks', 'time', 'ticks_amount',),
    'gethistory': (
        'login', 'order', 'symbol', 'open_price', 'close_price', 'profit', 'volume', 'open_time', 'close_time',
        'comment', 'commission', 'agent', 'cmd', 'sl', 'tp', 'swap',),
    'getgroups': ('group', 'currency', 'enable',),
    'getleverageandgroup': ('login', 'group', 'leverage',),
    'getmargininfo': ('login', 'balance', 'credit', 'equity', 'margin', 'free_margin',),
    'getorder': ('login', 'order', 'symbol', 'open_price', 'close_price', 'profit', 'volume', 'open_time', 'close_time',
                 'comment', 'commission', 'agent', 'cmd', 'sl', 'tp', 'swap'),
    'getopenedtradesforlogin': (
        'order', 'login', 'symbol', 'action', 'volume', 'open_time', 'open_price', 'sl', 'tp', 'profit', 'swaps',
        'comment',),
    'getopenpositionsforlogin': (
        'order', 'login', 'symbol', 'action', 'volume', 'open_time', 'open_price', 'sl', 'tp', 'profit', 'swaps',
        'comment',),
    'getopenpositions': (
        'position', 'login', 'symbol', 'action', 'volume', 'open_time', 'open_price', 'sl', 'tp', 'profit', 'swaps',),
    'getordershistory': (
        'login', 'order', 'symbol', 'activation_price', 'price_current', 'price_order', 'price_sl', 'price_tp',
        'price_trigger', 'state', 'volume_initial', 'volume_current', 'time_setup', 'time_done', 'time_expiration',
        'type',
        'contract_size', 'digits', 'external_id', 'position_id', 'type_fill', 'type_time', 'margin_rate', 'comment',
        'activation_mode',),
    'getpendingorders': ('order', 'login', 'symbol', 'order_type', 'open_time', 'open_price', 'sl', 'tp', 'comment', ),
    'getopenorders': ('login', 'order', 'symbol', 'open_price', 'current_price', 'profit', 'volume', 'open_time',
                      'close_time', 'comment', 'commission', 'agent_commission', 'cmd', 'sl', 'tp', 'swaps', ),
    'getmargininfoex': ('login', 'balance', 'credit', 'equity', 'margin', 'freeMargin',),
    'getsymbols': (
        'symbol_name', 'digits', 'profit_mode', 'security', 'swapLong', 'swapShort', 'spread', 'bid', 'ask', 'tickSize',
        'tickPrice', 'initial_margin', 'contract_size', 'point',),
    'gettradingvolume': (
        'login', 'order', 'symbol', 'price', 'profit', 'dealer', 'entry', 'volume', 'comment', 'commission',
        'profit_rate',),
    'getusersonline': ('login', 'ip',),
    'transferbalance': ('login', 'balance', 'credit', 'equity', 'margin', 'margin_free', 'success_flag',),
    'getjournal': ('param1', 'param2',),
    'getplugin': ('parameter_name', 'value',),

}
