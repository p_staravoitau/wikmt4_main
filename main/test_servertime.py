import web_api_mt4 as wik4
import allure
import pytest
from config import wik_mt4_addr


@allure.title("Запрос servertime: проверка работы.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_servertime_1():
    """
    Описание шагов:
        Setup: -
        Тест: У запроса нет параметров. В ответе возвращает время МТ4.
        Ожидаемый ответ: result=1&time=1624879483&request_id=
        Tear down: -
        """
    
    with allure.step('Запрос servertime'):
        servertime_1 = wik4.request(wik_mt4_addr, 'servertime')
        with allure.step('Проверка ответа'):
            assert '1' == servertime_1['result'] and int(servertime_1['time']) in range(1577836800, 1704067200)