import web_api_mt4 as wik4
import time
import allure
import pytest
from config import wik_mt4_addr

@pytest.fixture()
def accounts():
    for login in range(101, 104):
        with allure.step('Создать аккаунты'):
            wik4.request(wik_mt4_addr, 'createaccount', login=login, name='QA', group='demoforex', leverage=100)
        with allure.step('Начислить баланс на аккаунты'):
            wik4.request(wik_mt4_addr, 'changebalance', login=login, value='1000', comment='test_getallbalances')
    time.sleep(.100)
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        for login in range(101, 104):
            wik4.request(wik_mt4_addr,'deleteaccount', login=login)


@allure.title("Запрос getallbalances: проверка работы.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getallbalances_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунты 101, 102, 103
               - Начислить 1000 баланса на все аккаунты
        Тест:
            Шаг1: Отправить запрос getallbalances
        Tear down: - Удалить созданные аккаунты.
        """
    i = 0
    with allure.step('Запрос  getallbalances'):
        getallbalances_1 = wik4.request(wik_mt4_addr, 'getallbalances')
        with allure.step('Проверка ответа'):
            while int(getallbalances_1['answer'][i]['login']) < 101 or int(getallbalances_1['answer'][i]['login']) > 101:
                i = i + 1
            print(getallbalances_1['answer'][i])
            assert float(1000) == float(getallbalances_1['answer'][i]['balance'])

            while int(getallbalances_1['answer'][i]['login']) < 102 or int(getallbalances_1['answer'][i]['login']) > 102:
                i = i + 1
            print(getallbalances_1['answer'][i])
            assert float(1000) == float(getallbalances_1['answer'][i]['balance'])

            while int(getallbalances_1['answer'][i]['login']) < 103 or int(getallbalances_1['answer'][i]['login']) > 103:
                i = i + 1
            print(getallbalances_1['answer'][i])
            assert float(1000) == float(getallbalances_1['answer'][i]['balance'])

