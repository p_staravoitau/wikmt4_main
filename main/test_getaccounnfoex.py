import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100,
                     agent=12345, email='test123@test.com', id=54321, address='ul. Lenina', city='Moscow',
                     state='RUS', zipcode='223344', country='Russia', phone='9771234567', password='123456Qw',
                     password_phone='123456Qw', password_investor='123456Qw', comment='tets_getaccountinfo', enable=1,
                     enable_change_password=1, enable_read_only=0, send_reports=1, status='RE')
        with allure.step('Начисление баланса на аккаунт'):
            wik4.request(wik_mt4_addr, 'changebalance', login=101, value='1000', comment='test_getaccountinfoex')
        with allure.step('Начисление кредита на аккаунт'):
            wik4.request(wik_mt4_addr, 'changecredit', login=101, value='10', comment='test_getaccountinfoex')
    time.sleep(.100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)



@allure.title("Запрос getaccountinfoex: проверка работы.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getaccountinfoex_1(accounts):
    """
        Описание шагов:
            Setup: - Создать аккаунт с логином 101 с заполненной карточкой.
                   - Пополнить баланс на 1000.
                   - Пополнить кредит на 10.
            Тест:
                Шаг1: Отправить запрос getaccountinfoex: запрашиваем информацию по логину 101.
            Tear down: Удалить аккаунт.
    """
    with allure.step('Запрос  getaccountinfoex: запрашиваем информацию по логину 101.'):
        getaccountinfoex_1 = wik4.request(wik_mt4_addr, 'getaccountinfoex', login=101)
        print('Ответ Шаг1:\n', getaccountinfoex_1)
        with allure.step('Проверка ответа'):
            with allure.step('Проверка name'):
                assert 'QA' == getaccountinfoex_1['name']
            with allure.step('Проверка email'):
                assert 'test123@test.com' == getaccountinfoex_1['email']
            with allure.step('Проверка group'):
                assert 'demoforex' == getaccountinfoex_1['group']
            with allure.step('Проверка leverage'):
                assert '100' == getaccountinfoex_1['leverage']
            with allure.step('Проверка country'):
                assert 'Russia' == getaccountinfoex_1['country']
            with allure.step('Проверка state'):
                assert 'RUS' == getaccountinfoex_1['state']
            with allure.step('Проверка address'):
                assert 'ul. Lenina' == getaccountinfoex_1['address']
            with allure.step('Проверка phone'):
                assert '9771234567' == getaccountinfoex_1['phone']
            with allure.step('Проверка city'):
                assert 'Moscow' == getaccountinfoex_1['city']
            with allure.step('Проверка zipcode'):
                assert '223344' == getaccountinfoex_1['zip']
            with allure.step('Проверка enable'):
                assert '1' == getaccountinfoex_1['enable']
            with allure.step('Проверка tradingblocked'):
                assert '0' == getaccountinfoex_1['tradingblocked']
            with allure.step('Проверка balance'):
                assert float(1000) == float(getaccountinfoex_1['balance'])
            with allure.step('Проверка comment'):
                assert 'tets_getaccountinfo' == getaccountinfoex_1['comment']
            with allure.step('Проверка enableChangePassword'):
                assert '1' == getaccountinfoex_1['enableChangePassword']
            with allure.step('Проверка free_margin'):
                assert float(1010) == float(getaccountinfoex_1['free_margin'])
            with allure.step('Проверка opened_orders'):
                assert 'no' == getaccountinfoex_1['opened_orders']
            with allure.step('Проверка agent'):
                assert '12345' == getaccountinfoex_1['agent']
            with allure.step('Проверка id'):
                assert '54321' == getaccountinfoex_1['id']
            with allure.step('Проверка status'):
                assert 'RE' == getaccountinfoex_1['status']
            with allure.step('Проверка send_reports'):
                assert '1' == getaccountinfoex_1['send_reports']
            with allure.step('Проверка prevmonthbalance'):
                assert float(0) == float(getaccountinfoex_1['prevmonthbalance'])
            with allure.step('Проверка prevbalance'):
                assert float(0) == float(getaccountinfoex_1['prevbalance'])
            with allure.step('Проверка credit='):
                assert float(10) == float(getaccountinfoex_1['credit'])
            with allure.step('Проверка equity'):
                assert float(1010) == float(getaccountinfoex_1['equity'])
            with allure.step('Проверка margin'):
                assert float(0) == float(getaccountinfoex_1['margin'])
            with allure.step('Проверка margin_level'):
                assert float(0) == float(getaccountinfoex_1['margin_level'])
            with allure.step('Проверка taxes'):
                assert float(0) == float(getaccountinfoex_1['taxes'])


@allure.title("Запрос getaccountinfoex: несуществующий логин.")
@allure.severity('minor')
def test_getaccountinfoex_2():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос getaccountinfoex: запрашиваем информацию по несуществующиму логину
            Ожидаемый ответ: result=-4&reason=invalid user&login=100&request_id=
        Tear down: -
    """
    reason = "invalid user"
    with allure.step('Запрос  getaccountinfoex'):
        getaccountinfoex_1 = wik4.request(wik_mt4_addr, 'getaccountinfoex', login=101)
        print('Ответ Шаг1:\n', getaccountinfoex_1)
        with allure.step('Проверка ответа'):
            assert '-4' == getaccountinfoex_1['result'] and reason == getaccountinfoex_1['reason']
