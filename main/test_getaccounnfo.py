import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr



@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100,
                     agent=12345, email='test123@test.com', id=54321, address='ul. Lenina', city='Moscow',
                     state='RUS', zipcode='223344', country='Russia', phone='9771234567', password='123456Qw',
                     password_phone='123456Qw', password_investor='123456Qw', comment='tets_getaccountinfo', enable=1,
                     enable_change_password=1, enable_read_only=0, send_reports=1, status='RE')
        with allure.step('Начисление баланса на аккаунт'):
            wik4.request(wik_mt4_addr, 'changebalance', login=101, value='1000', comment='test_getaccountinfo')
    time.sleep(.100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)



@allure.title("Запрос getaccountinfo: проверка работы.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getaccountinfo_1(accounts):
    """
    Описание шагов:
        Setup: - Cоздать аккаунт с логином 101 с заполненной карточкой.
               - Пополнить баланс на 1000.
        Тест:
            Шаг1: Отправить запрос getaccountinfo: запрашиваем информацию по логину 101.
        Tear down: Удалить аккаунт.
    """
    with allure.step('Запрос getaccountinfo: запрашиваем текущую информацию по 101 аккаунту'):
        getaccountinfo_1 = wik4.request(wik_mt4_addr, 'getaccountinfo', login=101)
        print('Ответ Шаг1:\n', getaccountinfo_1)
        time.sleep(.100)
        with allure.step('Проверка ответа'):
            with allure.step('Проверка name'):
                assert 'QA' == getaccountinfo_1['name']
            with allure.step('Проверка email'):
                assert 'test123@test.com' == getaccountinfo_1['email']
            with allure.step('Проверка group'):
                assert 'demoforex' == getaccountinfo_1['group']
            with allure.step('Проверка leverage'):
                assert '100' == getaccountinfo_1['leverage']
            with allure.step('Проверка country'):
                assert 'Russia' == getaccountinfo_1['country']
            with allure.step('Проверка state'):
                assert 'RUS' == getaccountinfo_1['state']
            with allure.step('Проверка address'):
                assert 'ul. Lenina' == getaccountinfo_1['address']
            with allure.step('Проверка phone'):
                assert '9771234567' == getaccountinfo_1['phone']
            with allure.step('Проверка city'):
                assert 'Moscow' == getaccountinfo_1['city']
            with allure.step('Проверка zipcode'):
                assert '223344' == getaccountinfo_1['zip']
            with allure.step('Проверка enable'):
                assert '1' == getaccountinfo_1['enable']
            with allure.step('Проверка tradingblocked'):
                assert '0' == getaccountinfo_1['tradingblocked']
            with allure.step('Проверка balance'):
                assert float(1000) == float(getaccountinfo_1['balance'])
            with allure.step('Проверка comment'):
               assert 'tets_getaccountinfo' == getaccountinfo_1['comment']
            with allure.step('Проверка enableChangePassword'):
                assert '1' == getaccountinfo_1['enableChangePassword']
            with allure.step('Проверка free_margin'):
               assert float(1000) == float(getaccountinfo_1['free_margin'])
            with allure.step('Проверка opened_orders'):
               assert 'no' == getaccountinfo_1['opened_orders']
            with allure.step('Проверка agent'):
               assert '12345' == getaccountinfo_1['agent']


@allure.title("Запрос getaccountinfo: несуществующий логин.")
@allure.severity('minor')
def test_getaccountinfo_2():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос getaccountinfo по несуществующему логину
            Ожидаемый ответ: result=-4&reason=invalid user&login=101&request_id=
        Tear down: -
    """
    reason = "invalid user"
    with allure.step('Запрос  getaccountinfo'):
        getaccountinfo_1 = wik4.request(wik_mt4_addr, 'getaccountinfo', login=100)
        with allure.step('Проверка ответа'):
            assert '-4' == getaccountinfo_1['result'] and reason == getaccountinfo_1['reason']
