import web_api_mt4 as wik4
import pytest
import random
import time
import allure
from config import wik_mt4_addr, wik_mt4_addr_secret
import random_value as rand

random_group = rand.random_group(random.randint(6, 10))

@pytest.fixture()
def accounts():
    with allure.step('Создаем группу'):
        wik4.request(wik_mt4_addr, 'creategroup', name=random_group, enable=1)
        restart = wik4.request(wik_mt4_addr_secret, 'reboot')
    yield
    wik4.request(wik_mt4_addr_secret, 'deletegroup', group=random_group)

@allure.title("Запрос deletegroupswaps: удаление swaps для символа в группе")
@allure.severity('critical')
def test_deletegroupswaps_1(accounts):
    """
    Описание шагов:
        Setup: - Создать группу
        Тест:
            Шаг1: Отправить запрос modifygroupswaps для установки swaps символу EURUSD
            Шаг2: Отправить запрос deletegroupswaps для удаления swaps символа EURUSD
            Шаг3: Отправить запрос getgroupsymbolsinfo для проверки того, что swaps для EURUSD были удалены
        Tear down: удалить группу
    """

    with allure.step('Устанавливаем swaps для символа EURUSD в группе'):
        modifygroupswaps_1 = wik4.request(wik_mt4_addr, 'modifygroupswaps', name=random_group, symbol='EURUSD',
                                          swap_long='-0.15', swap_short='1.23', margin_divider=100)
        print('Ответ Шаг1:\n', modifygroupswaps_1)
        with allure.step('Проверка ответа'):
            assert '1' == modifygroupswaps_1['result']
    with allure.step('Запросом deletegroupswaps удаляем swaps символа EURUSD'):
        deletegroupswaps_1 = wik4.request(wik_mt4_addr, 'deletegroupswaps', name=random_group, symbol='EURUSD')
        print('Ответ Шаг2:\n', deletegroupswaps_1)
        with allure.step('Проверка ответа'):
            assert '1' == deletegroupswaps_1['result']
    time.sleep(3)
    with allure.step('Запрос getgroupsymbolsinfo: проверить, что swaps для EURUSD были удалены'):
        getgroupsymbolsinfo_1 = wik4.request(wik_mt4_addr, 'getgroupsymbolsinfo', group=random_group)
        print('Ответ Шаг3:\n', getgroupsymbolsinfo_1)
        with allure.step('Проверка ответа'):
            assert '1' == getgroupsymbolsinfo_1['result'] and '0' == getgroupsymbolsinfo_1['symbolsCount']

@allure.title("Запрос deletegroupswaps: удаление swaps для символа в группе, символ задан с ошибкой")
@allure.severity('critical')
def test_deletegroupswaps_2(accounts):
    """
    Описание шагов:
        Setup: - Создать группу
        Тест:
            Шаг1: Отправить запрос modifygroupswaps для установки swaps символу EURUSD
            Шаг2: Отправить запрос deletegroupswaps для удаления swaps символа EURUSDD, символа задан с ошибкой
            Ожидаемый ответ: result=-4&reason=no valid data on mt4 server
        Tear down: -
    """

    with allure.step('Запросом modifygroupswaps устанавливаем swaps для символа EURUSD в группе'):
        modifygroupswaps_1 = wik4.request(wik_mt4_addr, 'modifygroupswaps', name=random_group, symbol='EURUSD',
                                          swap_long='-0.15', swap_short='1.23', margin_divider=100)
        print('Ответ Шаг1:\n', modifygroupswaps_1)
        with allure.step('Проверка ответа'):
            assert '1' == modifygroupswaps_1['result']
    with allure.step('Запросом deletegroupswaps для удаления swaps символа EURUSD, символа задан с ошибкой'):
        deletegroupswaps_1 = wik4.request(wik_mt4_addr, 'deletegroupswaps', name=random_group, symbol='EURUSDD')
        print('Ответ Шаг2:\n', deletegroupswaps_1)
        with allure.step('Проверка ответа'):
            assert '-4' == deletegroupswaps_1['result']
