import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунты'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
        wik4.request(wik_mt4_addr, 'createaccount', login=102, name='QA', group='demoforex', leverage=100)
        time.sleep(0.1)
    with allure.step('Начисление баланса на аккаунт'):
        wik4.request(wik_mt4_addr, 'changebalance', login=101, value='1000', comment='test_getaccountbalance')
        wik4.request(wik_mt4_addr, 'changebalance', login=102, value='10000', comment='test_getaccountbalance')
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        for login in range(100, 103):
            wik4.request(wik_mt4_addr, 'deleteaccount', login=login)



@allure.title("Запрос getaccountbalance: проверка работы - один логин.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getaccountbalance_1(accounts):
    """
        Описание шагов:
            Setup: - Создание аккаунтов 101 и 102.
                    - Изменение баланса аккаунтов

            Тест:
                Шаг1: Запрос getaccountbalance: запрашиваем баланс аккаунта
            Tear down: Удалить аккаунты.
    """
    with allure.step('Запрос getaccountbalance'):
        getaccountbalance_1 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
        print('Ответ Шаг1:\n', getaccountbalance_1)
        with allure.step('Проверка ответа'):
            assert "1" == getaccountbalance_1['result'] and float(1000) == float(getaccountbalance_1['balance'])


@allure.title("Запрос getaccountbalance: проверка работы - несколько логинов")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getaccountbalance_2(accounts):
    """
       Описание шагов:
            Setup: - Создание аккаунтов 101 и 102.
                    - Изменение баланса аккаунтов
            Тест:
                Шаг1: Запрос getaccountbalance: запрашиваем баланс аккаунтов
            Tear down: Удалить аккаунты.
    """
    with allure.step('Запрос getaccountbalance'):
        getaccountbalance_1 = wik4.request(wik_mt4_addr, 'getaccountbalance', login="101;102")
        print('Ответ Шаг1:\n', getaccountbalance_1)
        with allure.step('Проверка ответа'):
            assert "1" == getaccountbalance_1['result'] and float(1000) == float(getaccountbalance_1['answer'][0]['balance']) \
                   and float(10000) == float(getaccountbalance_1['answer'][1]['balance'])


@allure.title("Запрос getaccountbalance: несуществующий аккаунт.")
@allure.severity('blocker')
def test_getaccountbalance_3():
    """
        Описание шагов:
            Setup: - Создание аккаунтов 101 и 102.
                    - Изменение баланса аккаунтов
            Тест:
                Шаг1: Запрос getaccountbalance: запрашиваем баланс несуществующего аккаунта
                Ожидаемый ответ: result=-4&reason=UserRecordsRequest ret nullptr&request_id=
            Tear down: Удалить аккаунты.
    """
    reason = 'UserRecordsRequest ret nullptr'
    with allure.step('Запрос getaccountbalance'):
        getaccountbalance_1 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=100)
        print('Ответ Шаг1:\n', getaccountbalance_1)
        with allure.step('Проверка ответа'):
            assert "-4" == getaccountbalance_1['result'] and reason == getaccountbalance_1['reason']
            