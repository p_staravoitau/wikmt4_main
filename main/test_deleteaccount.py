import web_api_mt4 as wik4
import pytest
import allure
from config import wik_mt4_addr, wik_mt4_addr_secret



@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    yield ()

@allure.title("Запрос deleteaccount: удаление существующего аккаунта")
@allure.severity('critical')
def test_deleteaccount_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос deleteaccount на удаления 101 аккаунта
        Tear down: -
    """

    with allure.step('Запрос deleteaccount на удаление 101 аккаунта'):
        deleteaccount_1 = wik4.request(wik_mt4_addr, 'deleteaccount', login='101')
        print('Ответ Шаг1\n', deleteaccount_1)
        with allure.step('Проверка ответа'):
            assert '1' == deleteaccount_1['result'] and '101' == deleteaccount_1['login']

@allure.title("Запрос deleteaccount: удаление несуществующего аккаунта")
@allure.severity('critical')
def test_deleteaccount_2():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос deleteaccount на удаления 101 аккаунта (несуществующего)
        Tear down: -
    """
    reason = 'invalid user'
    with allure.step('Запрос addorder на удаление несуществующего аккаунта'):
        deleteaccount_1 = wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        print('Ответ Шаг1\n', deleteaccount_1)
        with allure.step('Проверка ответа'):
            assert '-4' == deleteaccount_1['result'] and reason == deleteaccount_1['reason'] and '101' == deleteaccount_1['login']