import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунты'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
        wik4.request(wik_mt4_addr, 'createaccount', login=102, name='QA', group='demoforex', leverage=100)
    time.sleep(0.1)
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        for login in range(101, 103):
            wik4.request(wik_mt4_addr, 'deleteaccount', login=login)



@allure.title("Запрос bulkchangecredit: Попытка списать в минус.")
@allure.severity('critical')
@pytest.mark.smoke
def test_bulkchangecredit_1(accounts):
    """
    Описание шагов:
        Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangecredit на списание 10.000 с одного аккаунта
            Ожидаемый ответ: result=1&size=62
                            101;0.00;-1;0;"ERR: after operation credit will be negative"&request_id=
            Шаг2: отправить запрос bulkchangecredit на списание 10.000 с двух аккаунтов
            Ожидаемый ответ: result=1&size=123
                            101;0.00;-1;0;"ERR: after operation credit will be negative"
                            102;0.00;-1;0;"ERR: after operation credit will be negative"&request_id=
    Tear down: удаление аккаунтов
    """
    reason = '"ERR: after operation credit will be negative"'
    with allure.step('Списание баланса в минус - один аккаунт'):
        bulkchangecredit_minus_0 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins=101, value='-10000',
                                                comment='test_credit')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_minus_0['result'] \
                   and "0" == bulkchangecredit_minus_0['answer'][0]['success_flag'] \
                   and reason == bulkchangecredit_minus_0['answer'][0]['reason']
    with allure.step('Списание баланса в минус - перечень аккаунтов'):
        bulkchangecredit_minus_1 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='101;102', value='-10000',
                                                comment='test_credit')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_minus_1['result'] and "0" == bulkchangecredit_minus_1['answer'][0]['success_flag'] \
                   and "0" == bulkchangecredit_minus_1['answer'][1]['success_flag'] \
                   and reason == bulkchangecredit_minus_1['answer'][1]['reason']


@allure.title("Запрос bulkchangecredit: Начисление и списание баланса - один аккаунт")
@allure.severity('blocker')
@pytest.mark.smoke
def test_bulkchangecredit_2(accounts):
    """
    Описание шагов:
    Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangecredit на пополнение 10.000 одного аккаунта
            Ожидаемый ответ: result=1&size=27
                            101;10000.00;585908;1;"OK"&request_id=
            Шаг2: отправить запрос bulkchangecredit на списание 10.000 c аккаунта
            Ожидаемый ответ: result=1&size=23
                            101;0.00;585909;1;"OK"&request_id=
    Tear down: удаление аккаунтов
    """
    with allure.step('Запрос bulkchangecredit на пополнение аккаунта'):
        bulkchangecredit_101 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins=101, value='10000', comment='test_credit')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_101['result'] and float(10000) == float(bulkchangecredit_101['answer'][0]['newcredit'])
    with allure.step('Запрос bulkchangecredit на списание c аккаунта'):
        bulkchangecredit_101 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins=101, value='-10000', comment='test_credit')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_101['result'] and float(0) == float(bulkchangecredit_101['answer'][0]['newcredit'])


@allure.title("Запрос bulkchangecredit: Начисление и списание баланса - в перечне несколько аккаунтов (101,102)")
@allure.severity('blocker')
def test_bulkchangecredit_3(accounts):
    """
    Описание шагов:
        Setup: создать аккаунты с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangecredit на пополнение аккаунтов 101;102 на 10.000.
            Ожидаемый ответ: result=1&size=46
                            101;10000.00;585913;1;"OK"
                            102;10000.00;585914;1;"OK"&request_id=
            Шаг2: отправить запрос bulkchangecredit на списание c аккаунтов 101;102 10.000.
            Ожидаемый ответ: result=1&size=46
                            101;0.00;585915;1;"OK"
                            102;0.00;585916;1;"OK"&request_id=
    Tear down: удаление аккаунтов
    """
    with allure.step('Запрос bulkchangecredit на пополнение аккаунтов'):
        bulkchangecredit_101 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='101;102', value='10000', comment='test_credit')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_101['result'] and float(10000) == float(bulkchangecredit_101['answer'][0]['newcredit']) and float(10000) == float(bulkchangecredit_101['answer'][1]['newcredit'])
    with allure.step('Запрос bulkchangecredit на списание c аккаунта'):
        bulkchangecredit_101 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='101;102', value='-10000', comment='test_credit')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_101['result'] and float(0) == float(bulkchangecredit_101['answer'][0]['newcredit']) and float(0) == float(bulkchangecredit_101['answer'][1]['newcredit'])


@allure.title("Запрос bulkchangecredit: несуществующий аккаунт")
@allure.severity('minor')
def test_bulkchangecredit_4():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: отправить запрос bulkchangecredit на пополнение несуществующего аккаунта
            Ожидаемый ответ: result=-4&size=36
                            100;0.00;-1;0;"ERR: logins_notfound"&request_id=
    Tear down: -
    """
    reason = '"ERR: logins_notfound"'
    with allure.step('Запрос bulkchangecredit на пополнение несуществующего аккаунта'):
        bulkchangecredit_100 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins=100, value='10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '-4' == bulkchangecredit_100['result'] and "0" == bulkchangecredit_100['answer'][0]['success_flag'] \
                   and reason == bulkchangecredit_100['answer'][0]['reason']


@allure.title("Запрос bulkchangecredit: в перечне несуществующий аккаунт.")
@allure.severity('minor')
def test_bulkchangecredit_5(accounts):
    """
    Описание шагов:
        Setup: создать аккаунт с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangecredit на пополнение аккаунтов 100,101 на 10.000 (100 акк несуществует)
            Ожидаемый ответ: result=1&size=60
                            100;0.00;-1;0;"ERR: logins_notfound"&request_id=
                            101;10000.00;585915;1;"OK"
            Шаг2: отправить запрос bulkchangecredit в перечне только несуществующие логины
            Ожидаемый ответ: result=1&size=73
                            104;0.00;-1;0;"ERR: logins_notfound"
                            105;0.00;-1;0;"ERR: logins_notfound"&request_id=
    Tear down: удаление аккаунтов
        """
    with allure.step('Запрос bulkchangecredit на пополнение аккаунтов, в перечне один несущестаующий аккаунт'):
        bulkchangecredit_100 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='100;101', value='10000',
                                            comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '1' == bulkchangecredit_100['result'] and '1' == bulkchangecredit_100['answer'][0]['success_flag'] \
                   and float(10000) == float(bulkchangecredit_100['answer'][0]['newcredit']) and '0' == bulkchangecredit_100['answer'][1]['success_flag']
    with allure.step('Запрос bulkchangecredit на пополнение аккаунтов, в перечне два несуществующих аккаунта'):
        bulkchangecredit_100 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='104;105', value='10000',
                                            comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '-4' == bulkchangecredit_100['result'] and '0' == bulkchangecredit_100['answer'][0]['success_flag'] \
                   and '0' == bulkchangecredit_100['answer'][1]['success_flag']


@allure.title("Запрос bulkchangecredit: в перечне некоррекнтное значение.")
@allure.severity('critical')
def test_bulkchangecredit_6(accounts):
    """
    Описание шагов:
    Setup: создать аккаунт с логином 100, 102.
        Тест:
            Шаг1: отправить запрос bulkchangecredit на пополнение аккаунтам 10.000. В перечне некоррекнтное значение 'asd'
            Ожидаемый ответ: result=-3&reason=invalid params&request_id=
            Шаг2: отправить запрос bulkchangecredit в перечне только asd;99 - некорректное значение и не существующий аккаунт
            Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: удаление аккаунтов
    """
    reason = "invalid params"

    with allure.step('Запрос bulkchangecredit с некорректным значением в перечне'):
        bulkchangecredit_100 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='101;asd', value='10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '-3' == bulkchangecredit_100['result'] and reason == bulkchangecredit_100['reason']
    with allure.step('Запрос bulkchangecredit с некорректным значением и несуществующим аккаунтом в перечне'):
        bulkchangecredit_100 = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='asd;99', value='10000', comment='test_balance')
        with allure.step('Проверка ответа'):
            assert '-3' == bulkchangecredit_100['result'] and reason == bulkchangecredit_100['reason']


@allure.title("Запрос bulkchangecredit: некорректные значения в параметрах.")
@allure.severity('minor')
def test_bulkchangecredit_7():
    """
    Описание шагов:
        Setup: -
            Тест:
                Шаг1: отправить запрос bulkchangecredit на пополнение аккаунта на 10.000 с logins=asd.
                Шаг2: отправить запрос bulkchangecredit на пополнение аккаунта на 10.000 с value=asd.
                Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: -
    """

    reason = 'invalid params'

    with allure.step('Запрос bulkchangecredit с параметром logins=asd'):
        bulkchangecredit_login = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='asd', value=10000, comment='asd')
        assert '-3' == bulkchangecredit_login['result'] and reason == bulkchangecredit_login["reason"]
    with allure.step('Запрос bulkchangecredit с параметром value=asd'):
        bulkchangecredit_value = wik4.request(wik_mt4_addr, 'bulkchangecredit', logins='101', value='asd', comment='asd')
        assert '-3' == bulkchangecredit_value['result'] and reason == bulkchangecredit_value['reason']


@allure.title("Запрос bulkchangecredit: пустой комментарий.")
@allure.severity('minor')
def test_bulkchangecredit_8(accounts):
    """
    Описание шагов:
        Setup: создать аккаунт с логином 101, 102.
        Тест:
            Шаг1: отправить запрос bulkchangecredit на пополнение аккаунта 101 на 10.000 с comment=''.
            Ожидаемый ответ: result=-3&reason=invalid params&request_id=
        Tear down: удаление аккаунта
     """
    reason = 'invalid params'
    with allure.step('Запрос bulkchangecredit c пустым комментарием '):
        bulkchangebalance_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value=10000, comment='')
        assert '-3' == bulkchangebalance_login['result'] and reason == bulkchangebalance_login['reason']


@allure.title("Запрос bulkchangebalance: ошибка в обязательном параметры или его отсутствие.")
@allure.severity('minor')
def test_bulkchangebalance_9(accounts):
    """
    Описание шагов:
        Setup: создать аккаунт с логином 101, 102.
        Тест:
            Шаг1: ошибка в параметре login
            Шаг2: ошибка в параметре value
            Шаг3: ошибка в параметре comment
            Шаг4: отсутствует параметр comment
            Шаг5: отсутствует параметр value
            Шаг6: отсутствует параметр login
        Ожидаемый ответ: result=-3&reason=invalid params&request_id=
    Tear down: удаление аккаунтов
    """
    reason = 'invalid params'
    with allure.step('Запрос с ошибкой в параметре login'):
        bulkchangebalance_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', logns='101', value=10000, comment='asd')
        assert '-3' == bulkchangebalance_login['result'] and reason == bulkchangebalance_login['reason']
    with allure.step('Запрос с ошибкой в параметре value'):
        bulkchangebalance_value = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', valueeee=10000, comment='asd')
        assert '-3' == bulkchangebalance_value['result'] and reason == bulkchangebalance_value['reason']
    with allure.step('Запрос с ошибкой в параметре comment'):
        bulkchangebalance_comment = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value=10000, comnt='asd')
        assert '-3' == bulkchangebalance_comment['result'] and reason == bulkchangebalance_comment['reason']
    with allure.step('Запрос без параметра comment'):
        bulkchangebalance_no_comment = wik4.request(wik_mt4_addr, 'bulkchangebalance', logins='101', value=10000)
        assert '-3' == bulkchangebalance_no_comment['result'] and reason == bulkchangebalance_no_comment['reason']
    with allure.step('Запрос без параметра login'):
        bulkchangebalance_no_login = wik4.request(wik_mt4_addr, 'bulkchangebalance', value=10000, comment='asd')
        assert '-3' == bulkchangebalance_no_login['result'] and reason == bulkchangebalance_no_login['reason']
    with allure.step('Запрос без параметра value'):
        bulkchangebalance_no_value = wik4.request(wik_mt4_addr, 'bulkchangebalance', login='101', comment='asd')
        assert '-3' == bulkchangebalance_no_value['result'] and reason == bulkchangebalance_no_value['reason']