import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr,  wik_mt4_addr_secret


@pytest.fixture()
def delet_accounts():
    with allure.step('Подготовка сервера к тестам. Освобождаем диапазон с 100 по 110 аккаунты.'):
        for login in range(100, 111):
            getaccountinfo_0 = wik4.request(wik_mt4_addr, 'getaccountinfo', login=login)
            if getaccountinfo_0['result'] == '1':
                wik4.request(wik_mt4_addr, 'deleteaccount', login=login)


                
@allure.title("Подготовка сервера к тестам. ")
@allure.severity('accounts')
@pytest.mark.smoke
def test_0(delet_accounts):
    """Описание теста:
    Подготовка сервера к тестам:
            -  Проверка наличия аккаунтов на сервере.
            -  Проверка на них открытых ордеров/позиций.
            -  Их удаление/закрытие.
            -  Удаление аккаунтов.

    Известные ошибки:

    1. test_appendcomment_5
    2. test_bulkchangebalance_6
    3. test_bulkchangecredit_6
    4. test_bulkmodifyaccount_3
      test_bulkmodifyaccount_4
      test_bulkmodifyaccount_6
      test_bulkmodifyaccount_8
    5. test_bulkmodifydeal_3
      test_bulkmodifydeal_4
      test_bulkmodifydeal_6
      test_bulkmodifydeal_8
      test_bulkmodifydeal_9
    6. test_closeposition_3
      test_closeposition_4
      test_closeposition_5
      test_closeposition_6
      test_closeposition_8
    5. test_createaccount_2
      test_createaccount_4
      test_createaccount_5
      test_createaccount_6
      test_createaccount_7
    6. test_createaccountinrange_4
      test_createaccountinrange_5
      test_createaccountinrange_6
      test_createaccountinrange_7
    7. test_createaccountsinrange_4
      test_createaccountsinrange_5
      test_createaccountsinrange_6
      test_createaccountsinrange_7
    8. test_creategroup_5
    9. test_getaccountbalance_3
      test_getaccountbalance_5
      test_getaccountbalance_6
    10. test_getaccounts_1
    11. test_getcharthistory_1  (SCSV line в size)
      test_getcharthistory_2
      test_getcharthistory_5
    12. test_getdealshistory_3
      test_getdealshistory_4
      test_getdealshistory_5
      test_getdealshistory_6
    13. test_getloginsbygroup_2 (документация: Returns SCSV-formatted 'logins' list contained in the specified 'group' - в запросе не используется SCSV)
    14. test_getmargininfo_3    возвращает  free_margin в документации указано "freeMargin"
      test_getmargininfo_5
      test_getmargininfo_6
    15. test_getmargininfoex_1 (SCSV line в size)
      test_getmargininfoex_2
      test_getmargininfoex_3
      test_getmargininfoex_5
      test_getmargininfoex_6
    16. test_getordershistory_3
      test_getordershistory_4
      test_getordershistory_5
      test_getordershistory_6
    17. test_getplugin_3
    18. test_getusersonline 1 - F (SCSV line в size)
    19. test_gettradingvolume_3
      test_gettradingvolume_4
      test_gettradingvolume_5
      test_gettradingvolume_6
    20. test_modifyaccount_4
    21. test_modifygroup_5
    22. test_modifypending_2
      test_modifypending_3
    23. test_modifyposition_5
    24. test_openorder_4
      test_openorder_8
    25. test_transferbalance_2
      test_transferbalance_4

+ test_getaccountinfoex - разный формат ответа на enable_
enable=USER_RIGHT_ENABLED
enable_trading=1
enable_change_password=USER_RIGHT_PASSWORD
send_reports=USER_RIGHT_REPORTS

    """




