import web_api_mt4 as wik4
import pytest
import allure
import time
from config import wik_mt4_addr, wik_mt4_addr_secret

@pytest.fixture()
def addpluginparam():
    new_params = {'param_name': "TESTTEST", 'value': "test"}
    with allure.step('Запрос getplugin + deletepluginvalue: получаем текущие параметры плагина и полностью их удаляем'):
        getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        while getplugin_1['answer'][0]['parameter_name'] != '':
            wik4.request(wik_mt4_addr, 'deletepluginvalue', name='uber_feed.dll', param=getplugin_1['answer'][0]['parameter_name'])
            time.sleep(.200)
            getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
    with allure.step('Запрос updatepluginvalue на добавление параметра в uber_feed.dll'):
        wik4.request(wik_mt4_addr, 'updatepluginvalue', name='uber_feed.dll',  value=new_params['value'], param=new_params['param_name'])
    yield (new_params)


@allure.title("Запрос deletepluginvalue: проверка работы запроса")
@allure.severity('blocker')
@pytest.mark.smoke
def test_deletepluginvalue_1(addpluginparam):
    """
    Описание шагов:
        Setup: - Удалить все имеющиеся параметры в uber_feed.dll
               - Добавить параметр 'TESTTEST' с значением 'test' в uber_feed.dll
        Тест:
            Шаг1: Отправить запрос getplugin: получаем текущие параметры плагина
            Шаг2: Отправить запрос deletepluginvalue: удаляем параметр с именем 'TESTTEST'
            Шаг3: Отправить запрос getplugin: проверяем, что параметр был удален
        Tear down: -
    """
    with allure.step('Запрос getplugin: получаем текущие параметры плагина'):
        getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        print('Ответ Шаг1:\n', getplugin_1)
        assert getplugin_1['result'] == '1' and getplugin_1['answer'][0]['parameter_name'] == 'TESTTEST'
    with allure.step('Запрос deletepluginvalue: удаляем параметр с именем TESTTEST'):
        deletepluginvalue_1 = wik4.request(wik_mt4_addr, 'deletepluginvalue', name='uber_feed.dll', param=addpluginparam['param_name'])
        print('Ответ Шаг2:\n', deletepluginvalue_1)
        time.sleep(.100)
        assert deletepluginvalue_1['result'] == '1'
    with allure.step('Проверка удаленного параметра запросом getplugin'):
        getplugin_2 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        print('Ответ Шаг3:\n', getplugin_2)
        assert getplugin_2['result'] == '1' and getplugin_2['answer'][0]['parameter_name'] == ''

