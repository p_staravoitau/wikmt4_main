import web_api_mt4 as wik4
import pytest
import time
import allure
import random
import random_value as rand
from config import wik_mt4_addr, wik_mt4_addr_secret


@pytest.fixture()
def groups():
    with allure.step('Выбор имени для группы'):
        group_name = rand.random_group(random.randint(8, 12))
    with allure.step('Создаем новую группу по сгенерированному имени'):
        wik4.request(wik_mt4_addr, 'creategroup', name=group_name, currency='USD', enable=1)
        time.sleep(0.5)
    group = {'group_name': group_name}
    yield (group)
    wik4.request(wik_mt4_addr_secret, 'deleteaccount', login=101)
    wik4.request(wik_mt4_addr_secret, 'deletegroup', group=group_name)


@allure.title("Запрос getaccounts: проверка работы без агента")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getaccounts_1(groups):
    """
    Описание шагов:
        Setup: создаём группу
        Тест:
            Шаг1: Отправить запрос getaccounts без агента(аккаунтов в группе нет)
            Шаг2: Перезапустить сервер (для возможности создания аккаунтов в созданной группе)
            Шаг3: Отправить запрос createaccount для создания аккаунта 101 в группе
            Шаг4: Отправить запрос getaccounts для получения аккаунтов в группе без агента
        Tear down: - Удалить 101 аккаунт
                   - Удалить группу
    """
    with allure.step('Запрос getaccounts без агента (аккаунтов в группе нет)'):
        getaccounts_0 = wik4.request(wik_mt4_addr, 'getaccounts', group=groups['group_name'], agent=0)
        print('Ответ Шаг1:\n', getaccounts_0)
        with allure.step('Проверка ответа'):
            assert '1' == getaccounts_0['result'] and '' == getaccounts_0["answer"][0]['login']
    with allure.step('Запрос reboot: перезапускаем сервер'):
        reboot_1 = wik4.request(wik_mt4_addr_secret, 'reboot')
        print('Ответ Шаг2:\n', reboot_1)
        with allure.step('Проверка ответа'):
            assert '1' == reboot_1['result']
            time.sleep(5)
    with allure.step('Запрос createaccount: создаем аккаунт 101 в группе'):
        createaccount_101 = wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA',
                                         group=groups['group_name'], leverage=100,
                                         agent=12345, email='test123@test.com', id=54321, address='ul. Lenina',
                                         city='Moscow',
                                         state='RUS', zipcode='223344', country='Russia', phone='9771234567',
                                         password='123456Qw',
                                         password_phone='123456Qw', password_investor='123456Qw',
                                         comment='tets_getaccounts', enable=1,
                                         enable_change_password=1, enable_read_only=0, send_reports=1, status='RE')
        print('Ответ Шаг3:\n', createaccount_101)
        with allure.step('Проверка ответа'):
            assert '1' == createaccount_101['result']
    with allure.step('Запрос getaccounts: получение аккаунтов в группе (без агента)'):
        getaccounts_1 = wik4.request(wik_mt4_addr, 'getaccounts', group=groups['group_name'], agent=0)
        print('Ответ Шаг4:\n', getaccounts_1)
        with allure.step('Проверка ответа'):
            assert '1' == getaccounts_1['result'] and '101' == getaccounts_1["answer"][0]['login']
            with allure.step('Проверка name'):
                'QA' == getaccounts_1["answer"][0]['name']
            with allure.step('Проверка name'):
                'test123@test.com' == getaccounts_1["answer"][0]['email']
            with allure.step('Проверка leverage'):
                '100' == getaccounts_1["answer"][0]['leverage']
            with allure.step('Проверка country'):
                'Russia' == getaccounts_1["answer"][0]['country']
            with allure.step('Проверка state'):
                'RUS' == getaccounts_1["answer"][0]['state']
            with allure.step('Проверка adress'):
                'ul. Lenina' == getaccounts_1["answer"][0]['adress']
            with allure.step('Проверка city'):
                'Moscow' == getaccounts_1["answer"][0]['city']
            with allure.step('Проверка zip'):
                '223344' == getaccounts_1["answer"][0]['zip']
            with allure.step('Проверка enable'):
                '1' == getaccounts_1["answer"][0]['enable']
            with allure.step('Проверка balance'):
                float(0) == float(getaccounts_1["answer"][0]['balance'])
            with allure.step('Проверка comment'):
                'tets_getaccounts' == getaccounts_1["answer"][0]['comment']
            with allure.step('Проверка agent'):
                '12345' == float(getaccounts_1["answer"][0]['agent'])


@allure.title("Запрос getaccounts: проверка работы с агентом")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getaccounts_2(groups):
    """
    Описание шагов:
        Setup: создаём группу
        Тест:
            Шаг1: Отправить запрос getaccounts без агента(аккаунтов в группе нет)
            Шаг2: Перезапустить сервер (для возможности создания аккаунтов в созданной группе)
            Шаг3: Отправить запрос createaccount для создания аккаунта 101 в группе с агентом 12345
            Шаг4: Отправить запрос getaccounts для получения аккаунтов в группе с агентом 12346
            Шаг5: Отправить запрос getaccounts для получения аккаунтов в группе с агентом 12345
        Tear down: - Удалить 101 аккаунт
                   - Удалить группу
    """
    with allure.step('Запрос getaccounts без агента (аккаунтов в группе нет)'):
        getaccounts_0 = wik4.request(wik_mt4_addr, 'getaccounts', group=groups['group_name'], agent=0)
        print('Ответ Шаг1:\n', getaccounts_0)
        with allure.step('Проверка ответа'):
            assert '1' == getaccounts_0['result'] and '' == getaccounts_0["answer"][0]['login']
    with allure.step('Запрос reboot: перезапускаем сервер'):
        reboot_1 = wik4.request(wik_mt4_addr_secret, 'reboot')
        print('Ответ Шаг2:\n', reboot_1)
        with allure.step('Проверка ответа'):
            assert '1' == reboot_1['result']
            time.sleep(5)
    with allure.step('Запрос createaccount: создаем аккаунт 101 в группе'):
        createaccount_101 = wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA',
                                         group=groups['group_name'], leverage=100,
                                         agent=12345, email='test123@test.com', id=54321, address='ul. Lenina',
                                         city='Moscow',
                                         state='RUS', zipcode='223344', country='Russia', phone='9771234567',
                                         password='123456Qw',
                                         password_phone='123456Qw', password_investor='123456Qw',
                                         comment='tets_getaccounts', enable=1,
                                         enable_change_password=1, enable_read_only=0, send_reports=1, status='RE')
        print('Ответ Шаг3:\n', createaccount_101)
        with allure.step('Проверка ответа'):
            assert '1' == createaccount_101['result']
    with allure.step('Запрос  getaccounts: получение аккаунтов в группе с агнетом 12346'):
        getaccounts_1 = wik4.request(wik_mt4_addr, 'getaccounts', group=groups['group_name'], agent=12346)
        print('Ответ Шаг4:\n', getaccounts_1)
        with allure.step('Проверка ответа'):
            assert '1' == getaccounts_1['result'] and '' == getaccounts_1["answer"][0]['login']
    with allure.step('Запрос  getaccounts: получение акккаунтов в группе с агентом 12345'):
        getaccounts_2 = wik4.request(wik_mt4_addr, 'getaccounts', group=groups['group_name'], agent=12345)
        print('Ответ Шаг5:\n', getaccounts_2)
        with allure.step('Проверка ответа'):
            assert '1' == getaccounts_2['result'] and '101' == getaccounts_2["answer"][0]['login']
            with allure.step('Проверка name'):
                'QA' == getaccounts_2["answer"][0]['name']
            with allure.step('Проверка name'):
                'test123@test.com' == getaccounts_2["answer"][0]['email']
            with allure.step('Проверка leverage'):
                '100' == getaccounts_2["answer"][0]['leverage']
            with allure.step('Проверка country'):
                'Russia' == getaccounts_2["answer"][0]['country']
            with allure.step('Проверка state'):
                'RUS' == getaccounts_2["answer"][0]['state']
            with allure.step('Проверка adress'):
                'ul. Lenina' == getaccounts_2["answer"][0]['adress']
            with allure.step('Проверка city'):
                'Moscow' == getaccounts_2["answer"][0]['city']
            with allure.step('Проверка zip'):
                '223344' == getaccounts_2["answer"][0]['zip']
            with allure.step('Проверка enable'):
                '1' == getaccounts_2["answer"][0]['enable']
            with allure.step('Проверка balance'):
                float(0) == float(getaccounts_2["answer"][0]['balance'])
            with allure.step('Проверка comment'):
                'tets_getaccounts' == getaccounts_2["answer"][0]['comment']
            with allure.step('Проверка agent'):
                '12345' == float(getaccounts_2["answer"][0]['agent'])

@allure.title("Запрос getaccounts: несуществующая группа.")
@allure.severity('minor')
def test_getaccounts_3():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос getaccounts с несуществующей группой
            Ожидаемый ответ: result=-4&reason=invalid group&request_id=
        Tear down: -
    """
    group_name = 'klklklklkklk123'
    reason = 'invalid group'
    with allure.step('Запрос  getaccounts с несуществующей группой'):
        getaccounts_0 = wik4.request(wik_mt4_addr, 'getaccounts', group=group_name, agent=0)
        print('Ответ Шаг1:\n', getaccounts_0)
        with allure.step('Проверка ответа'):
            assert '-4' == getaccounts_0['result'] and reason == getaccounts_0['reason']