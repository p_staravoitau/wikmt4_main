import web_api_mt4 as wik4
import pytest
import time
import allure
import random
from config import wik_mt4_addr, wik_mt4_addr_secret


@pytest.fixture()
def order_history():
    with allure.step('Создать аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100,
                     password='123456Qw', password_investor='123456Qw')
        with allure.step('Начисление баланса на аккаунт'):
            wik4.request(wik_mt4_addr, 'changebalance', login=101, value='10000', comment='test_changeagentcommission')
            time.sleep(.100)
        with allure.step('Открываем позиции'):
            wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=0.5000, ask=0.60008)
            open_time = wik4.request(wik_mt4_addr, 'servertime')
            openorder_cmd0 = wik4.request(wik_mt4_addr, 'addorder', login='101', cmd='0', symbol='EURUSD', volume=100,
                                          open_time=open_time['time'])
            time.sleep(.100)

    order = {'order': openorder_cmd0['order']}
    yield (order)
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)



@allure.title("Запрос changeagentcommission: проверка работы.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_changeagentcommission_1(order_history):
    """
        Описание шагов:
        Setup: создать аккаунт с логином 101.
            - Пополняем баланс аккаунта
            - Открываем на нём позицию.
            - Получаем значение ордера для передачи в тест.
        Тест:
            Шаг1: Рассчитываем новую комиссию для сделки
            Шаг2: Устанавливаем новую рандомную комиссию
            Шаг3: Запрашиваем историю по необходимому ордеру и сверяем данные
        Tear down:
            - Удаление аккаунта
    """
    with allure.step('Расчитываем новую агентскую комиссию для ордера'):
        new_comission = str(random.randint(5, 150))
    with allure.step('Запросом changeagentcommission устанавливаем комиссию'):
        wik4.request(wik_mt4_addr, 'changeagentcommission', order=order_history['order'], value=new_comission)
        with allure.step('Запрашиваем историю по необходиму ордеру'):
            getorder_1 = wik4.request(wik_mt4_addr, 'getorder', **{'login': 101, 'order': order_history['order']})
            print('Ответ Шаг3:\n', getorder_1)
            with allure.step('Проверка ответа'):
                assert float(getorder_1['answer'][0]['agent']) == float(new_comission)



@allure.title("Запрос changeagentcommission: несуществующая сделка.")
@allure.severity('minor')
def test_changeagentcommission_2():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: вычисление нового №сделки
            Шаг2: Попытка отправить запрос на установление комиссии на несуществующей сделки
            Шаг3: проверяем ответ
            Ожидаемый ответ: &result=-4&reason=no trades&request_id=:
        Tear down: -
    """
    reason = "no trades"
    with allure.step('Расчитываем новый № сделки'):
        new_order = str(int(random.randint(15415, 150000)) + int(random.randint(52456, 12345678)))
        print('Ответ Шаг1:\n', new_order)
    with allure.step('Запросом changeagentcommission устанавливаем комиссию'):
        changeagentcommission = wik4.request(wik_mt4_addr,'changeagentcommission', order=new_order, value=7)
        print('Ответ Шаг2:\n', changeagentcommission)
        with allure.step('Проверяем ответ на запроса'):
            assert '-4' == changeagentcommission['result'] and reason == changeagentcommission['reason']


@allure.title("Запрос changeagentcommission: некорректные значения в параметрах.")
@allure.severity('minor')
def test_changeagentcommission_3(order_history):
    """
    Описание шагов:
        Setup: создать аккаунт с логином 101.
            - Пополнить баланс аккаунта
            - Открыть на нём позицию.
            - Запрсоом getorder находим номер нужной сделки.
        Тест:
            Шаг1: отправить запрос changeagentcommission с некорректными данными в order
            Шаг2: отправить запрос changeagentcommission с некорректными данными в value
            Ожидаемый ответ: result=-3&reason=parsing_problem&request_id=
        Tear down:
            - Закрытие сделки
            - Удаление аккаунта
    """
    reason = 'parsing_problem'
    with allure.step('Запрос changeagentcommission с некорректными данными в order'):
        changeagentcommission_order = wik4.request(wik_mt4_addr,'changeagentcommission', order='asd', value=10)
        print('Ответ Шаг1:\n', changeagentcommission_order)
    with allure.step('Запрос changeagentcommission с некорректными данными в value'):
        changeagentcommission_value = wik4.request(wik_mt4_addr,'changeagentcommission', order =order_history['order'], value='asd')
        print('Ответ Шаг2:\n', changeagentcommission_value)
    with allure.step('Проверяем ответ'):
        assert '-3' == changeagentcommission_order['result'] and reason == changeagentcommission_order['reason']
        assert '-3' == changeagentcommission_value['result'] and reason == changeagentcommission_value['reason']


@allure.title("Запрос changeagentcommission: некорректные обязательные параметры или их отсутствие.")
@allure.severity('minor')
def test_changeagentcommission_4(order_history):
    """
    Описание шагов:
        Setup: создаём аккаунт с логином 101.
            - Пополняем баланс аккаунта
            - Открываем на нём позицию.
            - Запрсоом getorder находим номер нужной сделки.
        Тест:
            Шаг1: отправляем запрос changeagentcommission с ошибкой в параметре order
            Шаг2: отправляем запрос changeagentcommission с ошибкой в параметре value
            Шаг3: отправляем запрос changeagentcommission без параметра order
            Шаг4: отправляем запрос changeagentcommission без параметра value
            Ожидаемый ответ: reason=invalid params (required params are (UInt64)order, (Double)value)&result=-3&size=81
        Tear down:
            - Закрытие сделки
            - Удаление аккаунта
    """
    reason = "invalid params"
    with allure.step('Запрос changeagentcommission с ошибкой в параметре order'):
        changeagentcommission_order = wik4.request(wik_mt4_addr,'changeagentcommission', de=order_history['order'], value= 10)
        print('Ответ Шаг1:\n', changeagentcommission_order)
        with allure.step('Проверяем ответ'):
            assert '-3' == changeagentcommission_order['result'] and reason == changeagentcommission_order['reason']
    with allure.step('Запрос changeagentcommission с ошибкой в параметре value'):
        changeagentcommission_value= wik4.request(wik_mt4_addr,'changeagentcommission', order=order_history['order'], vue='10')
        print('Ответ Шаг2:\n', changeagentcommission_value)
        with allure.step('Проверяем ответ'):
            assert '-3' == changeagentcommission_value['result'] and reason == changeagentcommission_value['reason']
    with allure.step('Зпрос changeagentcommission без параметра order'):
        changeagentcommission_no_order = wik4.request(wik_mt4_addr,'changeagentcommission', value= 10)
        print('Ответ Шаг3:\n', changeagentcommission_no_order)
        with allure.step('Проверяем ответ'):
            assert '-3' == changeagentcommission_no_order['result'] and reason == changeagentcommission_no_order['reason']
    with allure.step('Запрос changeagentcommission без параметра value'):
        changeagentcommission_no_value= wik4.request(wik_mt4_addr,'changeagentcommission', order=order_history['order'],)
        print('Ответ Шаг4:\n', changeagentcommission_no_value)
        with allure.step('Проверяем ответ'):
            assert '-3' == changeagentcommission_no_value['result'] and reason == changeagentcommission_value['reason']