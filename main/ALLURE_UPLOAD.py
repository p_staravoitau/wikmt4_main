import requests
import shutil
import os
import datetime
import time
import sys
from config import version

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def get_upload_link(url, token):
    resp = requests.get(
        url, headers={'Authorization': 'Token {token}'. format(token=token)}
    )
    return resp.json()

if __name__ == "__main__":
    # replace with your token
    token = '7e1da7bd822b5cba3e3c92344a25670880f66c68'
    # replace with your library id
    upload_link = get_upload_link(
        'https://cloud.t4b.com/api2/repos/a2e3e569-e65d-4913-b6e1-30ca9cec77c0/upload-link/', token
    )
    try:
        zip_name = 'WIKMT4_v' + version + '_' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        zipcreate = shutil.make_archive(zip_name, 'zip', path + '\\allure_results')
        print('Архив под названием ' + zip_name + ' успешно создан в папке проекта.')
    except:
        print('Неудача при создании архива.\n'
              'Проверьте, сгенерировали ли вы отчеты Allure в папке allure_results в корневой папке проекта')
        sys.exit()

    response = requests.post(
        upload_link, data={'parent_dir': '/Allure_Reports/WIKMT4_main'},
        files={'file': open(zip_name + '.zip', 'rb')},
        headers={'Authorization': 'Token {token}'. format(token=token)}
    )

    if response.status_code == 200:
        print('Архив ' + zip_name + '.zip успешно отправлен на Cloud.\n' + str(response))
        print('Путь к архиву: https://cloud.t4b.com/#group/3/lib/a2e3e569-e65d-4913-b6e1-30ca9cec77c0/Allure_Reports/WIKMT4_main')
    else:
        print('!!! Произошла ошибка заливки .zip на Cloud. !!!')
        print(response)

    print('\nУдаление архива из локальной директории проекта через 3 сек...')
    time.sleep(3)
    #removal of the generated file from the project directory
    path2 = os.path.join(os.path.abspath(os.path.dirname(__file__)), zip_name + '.zip')
    os.remove(path2)
    print('Архив ' + zip_name + '.zip успешно удален.')