import web_api_mt4 as wik4
import pytest
import allure
import time
import random
import random_value as rand
from config import wik_mt4_addr, wik_mt4_addr_secret

@pytest.fixture()
def accounts():
    with allure.step('Создать аккаунты'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    with allure.step('Начислить баланс на аккаунты'):
        wik4.request(wik_mt4_addr, 'changebalance', login=101, value='10000', comment='test_openpending')
    time.sleep(.100)
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)


@allure.title("Запрос modifypending: изменение pending ордера - обязательные параметры")
@allure.severity('blocker')
@pytest.mark.smoke
def test_modifypending_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунты 101
               - Начислить 10000 баланса на 101 аккаунт
        Тест:
            Шаг1: Вбрасываем котировку для EURUSD
            Шаг2: Отправить запрос openpending: все обязательные параметры
            Шаг3: Запрос getpendingorders: получение pending ордеров на аккаунте
            Шаг4: Запрос modifypending: изменение price у pending ордера
            Шаг5: Запрос getpendingorders: получение информации о pending ордере на аккауннте
        Tear down: Удаление аккаунта
    """
    random_volume = rand.random_volume()
    random_price_1 = round(random.uniform(1.1, 1.5), 5)
    random_price_2 = round(random.uniform(1.1, 1.5), 5)
    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.00000, ask=1.00008)
        print('Ответ Шаг1:\n', pushquote_EURUSD)
        assert pushquote_EURUSD['result'] == '1' and 'success' == pushquote_EURUSD['reason']
        time.sleep(2)
    with allure.step('Запрос openpending со всеми обязательными параметрами'):
        openpending_1 = wik4.request(wik_mt4_addr, 'openpending', login=101, symbol='EURUSD', cmd=4,
                                     volume=random_volume, price=random_price_1)
        print('Ответ Шаг2:\n', openpending_1)
        assert openpending_1['result'] == '1'
    with allure.step('Запрос getpendingorders: получение pending ордеров на аккаунте'):
        getpendingorders_1 = wik4.request(wik_mt4_addr, 'getpendingorders', login=101)
        print('Ответ Шаг3:\n', getpendingorders_1)
        assert getpendingorders_1['result'] == '1' and getpendingorders_1['trades'] == '1'\
               and getpendingorders_1['answer'][0]['order'] == openpending_1['order'] \
               and float(getpendingorders_1['answer'][0]['open_price']) == float(random_price_1) \
               and getpendingorders_1['answer'][0]['symbol'] == 'EURUSD' \
               and float(getpendingorders_1['answer'][0]['sl']) == float(0) \
               and float(getpendingorders_1['answer'][0]['tp']) == float(0) \
               and getpendingorders_1['answer'][0]['comment'] == ''
    with allure.step('Запрос modifypending: изменение price у pending ордера'):
        modifypending_1 = wik4.request(wik_mt4_addr, 'modifypending',  order=getpendingorders_1['answer'][0]['order'],
                                       price=random_price_2)
        print('Ответ Шаг4:\n', modifypending_1)
        assert modifypending_1['result'] == '1'
    with allure.step('Запрос getpendingorders: получение информации о pending ордере на аккауннте'):
        getpendingorders_2 = wik4.request(wik_mt4_addr, 'getpendingorders', login=101)
        print('Ответ Шаг5:\n', getpendingorders_2)
        assert getpendingorders_2['result'] == '1' and getpendingorders_2['trades'] == '1' \
               and getpendingorders_2['answer'][0]['order'] == openpending_1['order'] \
               and float(getpendingorders_2['answer'][0]['open_price']) == float(random_price_2) \
               and getpendingorders_2['answer'][0]['symbol'] == 'EURUSD' \
               and float(getpendingorders_2['answer'][0]['sl']) == float(0) \
               and float(getpendingorders_2['answer'][0]['tp']) == float(0) \
               and getpendingorders_2['answer'][0]['comment'] == ''


@allure.title("Запрос modifypending: изменение pending ордера - обязательные + доп. параметры")
@allure.severity('blocker')
@pytest.mark.smoke
def test_modifypending_2(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунты 101
               - Начислить 10000 баланса на 101 аккаунт
        Тест:
            Шаг1: Вбрасываем котировку для EURUSD
            Шаг2: Отправить запрос openpending: все обязательные параметры
            Шаг3: Запрос getpendingorders: получение pending ордеров на аккаунте
            Шаг4: Запрос modifypending: изменение price, sl, tp у pending ордера
            Шаг5: Запрос getpendingorders: получение информации о pending ордере на аккауннте
        Tear down: Удаление аккаунта
    """
    random_volume = rand.random_volume()
    random_price_1 = round(random.uniform(1.1, 1.2), 5)
    random_price_2 = round(random.uniform(1.1, 1.2), 5)
    random_sl = round(random.uniform(0.9, 1), 5)
    random_tp = round(random.uniform(1.3, 2), 5)
    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.00000, ask=1.00008)
        print('Ответ Шаг1:\n', pushquote_EURUSD)
        assert pushquote_EURUSD['result'] == '1' and 'success' == pushquote_EURUSD['reason']
        time.sleep(2)
    with allure.step('Запрос openpending со всеми обязательными параметрами'):
        openpending_1 = wik4.request(wik_mt4_addr, 'openpending', login=101, symbol='EURUSD', cmd=4,
                                     volume=random_volume, price=random_price_1)
        print('Ответ Шаг2:\n', openpending_1)
        assert openpending_1['result'] == '1'
    with allure.step('Запрос getpendingorders: получение pending ордеров на аккаунте'):
        getpendingorders_1 = wik4.request(wik_mt4_addr, 'getpendingorders', login=101)
        print('Ответ Шаг3:\n', getpendingorders_1)
        assert getpendingorders_1['result'] == '1' and getpendingorders_1['trades'] == '1'\
               and getpendingorders_1['answer'][0]['order'] == openpending_1['order'] \
               and float(getpendingorders_1['answer'][0]['open_price']) == float(random_price_1) \
               and getpendingorders_1['answer'][0]['symbol'] == 'EURUSD' \
               and float(getpendingorders_1['answer'][0]['sl']) == float(0) \
               and float(getpendingorders_1['answer'][0]['tp']) == float(0) \
               and getpendingorders_1['answer'][0]['comment'] == ''
    with allure.step('Запрос modifypending: изменение price, sl, tp у pending ордера'):
        modifypending_1 = wik4.request(wik_mt4_addr, 'modifypending',  order=getpendingorders_1['answer'][0]['order'],
                                       price=random_price_2, sl=random_sl, tp=random_tp)
        print('Ответ Шаг4:\n', modifypending_1)
        assert modifypending_1['result'] == '1'
    with allure.step('Запрос getpendingorders: получение информации о pending ордере на аккауннте'):
        getpendingorders_2 = wik4.request(wik_mt4_addr, 'getpendingorders', login=101)
        print('Ответ Шаг5:\n', getpendingorders_2)
        assert getpendingorders_2['result'] == '1' and getpendingorders_2['trades'] == '1' \
               and getpendingorders_2['answer'][0]['order'] == openpending_1['order'] \
               and float(getpendingorders_2['answer'][0]['open_price']) == float(random_price_2) \
               and getpendingorders_2['answer'][0]['symbol'] == 'EURUSD' \
               and float(getpendingorders_2['answer'][0]['sl']) == float(random_sl) \
               and float(getpendingorders_2['answer'][0]['tp']) == float(random_tp) \
               and getpendingorders_2['answer'][0]['comment'] == ''


