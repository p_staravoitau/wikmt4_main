import socket
from mt4_schemes import mt4_schemes
import logging


request_default = {}
request_raw = b''

response_default = {}
response_raw = b''

def request(addr, action, **kwargs):
    try:
        sock = socket.socket()
        sock.connect(addr)
    except ConnectionRefusedError as e:
        logging.error("Невозможно установить соединение с WIK! Возможно вы указали неверный IP адрес WIK или не запустили его")

        raise e
    request = MT5Request(action, request_default, request_raw, kwargs)
    sock.send(bytes_t(request))
    schemes = mt4_schemes
    scheme = schemes[action] if action in schemes else None
    response = MT5Response(recvall(sock), scheme)
    sock.close()
    return response


def recvall(sock):
    data_length = int(sock.recv(8).decode())
    data = b''
    while len(data) != data_length:
        data += sock.recv(data_length)
    return data


def string_t(default):
    return '&'.join(['{0}={1}'.format(k, v) for k, v in default.items()])


def bytes_t(default):
    return bytes(string_t(default), encoding='utf-8')


def MT5Request(action, request_default, request_raw, params):
    request_default = {'action': action}
    request_default.update(params)
    string_b = string_t(request_default)
    request_raw = bytes_t(request_default)
    logging.debug("Отправляется реквест {}".format(request_raw))
    return request_default


def MT5Response(byte_string=None, scheme=None, ):
    response_raw = byte_string
    logging.debug("Получен ответ {}".format(response_raw))
    string = str(byte_string, encoding='utf-8').split('&')
    p = scheme
    response_default = {k: v for k, v in [x.split('=', 1) for x in string if x != '']}

    if ('trades' in response_default and 'size' in response_default) or 'symbolsCount' in response_default \
            or ('size' in response_default and scheme != None):

        new_answer = []
        answer = response_default['size'].split('\n')
        answer.pop(0)

        for item in answer:
            item = item.split(';')
            data = {k: v for k, v in zip(scheme, item)}
            new_answer.append(data)

        response_default['answer'] = new_answer
    return response_default
