import web_api_mt4 as wik4
import allure
import pytest
from config import wik_mt4_addr
import re


@allure.title("Запрос getusersonline: проверка работы запроса.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getusersonline_1():
    """
    Описание шагов:
        Тест: у запроса нет параметров.
        Ожидаемый результат:result=1&count=1
                            1;192.168.56.101&request_id=
        Tear down:
    """
    with allure.step('Запрос getusersonline'):
        getusersonline = wik4.request(wik_mt4_addr, 'getusersonline')
        with allure.step('Проверка ответа'):
            print(getusersonline)
            assert '1' == getusersonline['result'] 