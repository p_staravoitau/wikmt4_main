import web_api_mt4 as wik4
import pytest
import allure
import time
from config import wik_mt4_addr, wik_mt4_addr_secret


@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        time.sleep(0.1)


@allure.title("Запрос deletependingorder: удаление размещенного pending ордера")
@allure.severity('critical')
def test_deletependingorder_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос addorder на размещение pending ордера
            Шаг2: Отправить запрос deletependingorder для удаления pending ордера
            Шаг3: Отправить запрос gethistory для проверки, что ордер действительно был удален
        Tear down: Удалить аккаунт
    """
    reason = 'no open pendings'
    with allure.step('Запрос addorder на пополнение баланса 101 аккаунту'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=1, cmd=2, open_time=current_time_1['time'])
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result']
    with allure.step('Запрос deletependingorder для удаления ордера'):
        deletependingorder_1 = wik4.request(wik_mt4_addr, 'deletependingorder', order=addorder_1['order'])
        print('Ответ Шаг2:\n', deletependingorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == deletependingorder_1['result']
    with allure.step('Запрос getpendingorders: проверка, что ордер действительно был удален'):
        getpendingorders_1 = wik4.request(wik_mt4_addr, 'getpendingorders', login='101')
        print('Ответ Шаг3:\n', getpendingorders_1)
        with allure.step('Проверка ответа'):
            assert '-4' == getpendingorders_1['result'] and reason == getpendingorders_1['reason']



@allure.title("Запрос deletependingorder: удаление нескольких размещенных pending ордеров")
@allure.severity('critical')
def test_deletependingorder_2(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос addorder на размещение двух pending ордеров
            Шаг2: Отправить запрос deletependingorder для удаления двух pending ордеров
            Шаг3: Отправить запрос gethistory для проверки, что ордера действительно былы удалены
        Tear down: Удалить аккаунт
    """
    reason = 'no open pendings'
    with allure.step('Запрос addorder на пополнение баланса 101 аккаунту'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=1, cmd=2, open_time=current_time_1['time'])
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=2, cmd=2, open_time=current_time_1['time'])
        print('Ответ Шаг1:\n', addorder_1, addorder_2)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result'] and '1' == addorder_2['result']
    with allure.step('Запрос deletependingorder для удаления ордера'):
        deletependingorder_1 = wik4.request(wik_mt4_addr, 'deletependingorder', order=addorder_1['order'] + ';' + addorder_2['order'])
        print('Ответ Шаг2:\n', deletependingorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == deletependingorder_1['result']
    with allure.step('Запрос getpendingorders: проверка, что ордер действительно был удален'):
        getpendingorders_1 = wik4.request(wik_mt4_addr, 'getpendingorders', login='101')
        print('Ответ Шаг3:\n', getpendingorders_1)
        with allure.step('Проверка ответа'):
            assert '-4' == getpendingorders_1['result'] and reason == getpendingorders_1['reason']


@allure.title("Запрос deletependingorder: удаление нескольких pending ордеров, один из которых - несуществующий")
@allure.severity('normal')
def test_deletependingorder_3(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос addorder на размещение pending ордера
            Шаг2: Отправить запрос deletependingorder для удаления нескольких pending ордеров (один ордер несуществует)
            Шаг3: Отправить запрос gethistory для проверки, что ордер действительно был удален
        Tear down: Удалить аккаунт
    """
    reason = 'no open pendings'
    with allure.step('Запрос addorder на пополнение баланса 101 аккаунту'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=1, cmd=2, open_time=current_time_1['time'])
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result']
    with allure.step('Запрос deletependingorder для удаления ордера'):
        deletependingorder_1 = wik4.request(wik_mt4_addr, 'deletependingorder', order=addorder_1['order']+";777777")
        print('Ответ Шаг2:\n', deletependingorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == deletependingorder_1['result']  and '777777' == deletependingorder_1['answer'][0]['order_no_delete']
    with allure.step('Запрос getpendingorders: проверка, что ордер действительно был удален'):
        getpendingorders_1 = wik4.request(wik_mt4_addr, 'getpendingorders', login='101')
        print('Ответ Шаг3:\n', getpendingorders_1)
        with allure.step('Проверка ответа'):
            assert '-4' == getpendingorders_1['result'] and reason == getpendingorders_1['reason']


@allure.title("Запрос deletependingorder: удаление неcуществующего ордера")
@allure.severity('normal')
def test_deletependingorder_4():
    """
    Описание шагов:
        Setup: -
        Тест:
            Шаг1: Отправить запрос deletependingorder для удаления несущестсующего ордера
            Ожидаемый ответ: result=1&size=6\n777777&request_id=
        Tear down: -
    """

    reason = 'no trades'
    with allure.step('Запрос deletependingorder для удаления несуществующего ордера'):
        deletependingorder_1 = wik4.request(wik_mt4_addr, 'deletependingorder', order=777777)
        print('Ответ Шаг1:\n', deletependingorder_1)
        with allure.step('Проверка ответа'):
            assert '-4' == deletependingorder_1['result'] and reason == deletependingorder_1['reason']



@allure.title("Запрос deletependingorder: попытка удаления обычного ордера")
@allure.severity('critical')
def test_deletependingorder_5(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
        Тест:
            Шаг1: Отправить запрос addorder на размещение обычного (cmd=0) ордера
            Шаг2: Отправить запрос deletependingorder для удаления pending ордера
            Шаг3: Отправить запрос gethistory для проверки, что ордер действительно был удален
        Tear down: Удалить аккаунт
    """
    reason = 'no open pendings'
    with allure.step('Запрос addorder на пополнение баланса 101 аккаунту'):
        current_time_1 = wik4.request(wik_mt4_addr, 'servertime')
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=1, cmd=0, open_time=current_time_1['time'])
        print('Ответ Шаг1:\n', addorder_1)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result']
    with allure.step('Запрос deletependingorder для удаления ордера'):
        deletependingorder_1 = wik4.request(wik_mt4_addr, 'deletependingorder', order=addorder_1['order'])
        print('Ответ Шаг2:\n', deletependingorder_1)
        with allure.step('Проверка ответа'):
            assert '-3' == deletependingorder_1['result']
    with allure.step('Запрос getpendingorders: проверка, что ордер действительно был удален'):
        getpendingorders_1 = wik4.request(wik_mt4_addr, 'getpendingorders', login='101')
        print('Ответ Шаг3:\n', getpendingorders_1)
        with allure.step('Проверка ответа'):
            assert '-4' == getpendingorders_1['result'] and reason == getpendingorders_1['reason']
