import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr

@pytest.fixture()
def accounts():
    with allure.step('Создать аккаунты c историей пополнения баланса'):
        with allure.step('Создать аккаунты'):
            wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
            wik4.request(wik_mt4_addr, 'createaccount', login=102, name='QA', group='demoforex', leverage=100)
            time.sleep(0.1)
        with allure.step('Начислить баланса на аккаунт'):
            changebalance_1 = wik4.request(wik_mt4_addr, 'changebalance', login=101, value='10000', comment='test_balancefix')
            changebalance_2 = wik4.request(wik_mt4_addr, 'changebalance', login=102, value='10000', comment='test_balancefix')
        with allure.step('Удалить ордера о пополнении балансов'):
            wik4.request(wik_mt4_addr, 'deleteorder', order=changebalance_1['orderid'])
            wik4.request(wik_mt4_addr, 'deleteorder', order=changebalance_2['orderid'])
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        for login in range(101, 103):
            wik4.request(wik_mt4_addr, 'deleteaccount', login=login)



@allure.title("Запрос balancefix: проверка работы - один логин.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_balancefix_1(accounts):
    """
        Описание шагов:
            Setup: Создать аккаунты 101 и 102.
                - Пополнить баланс аккаунтам на 10.000
                - Удалить ордеров о пополнении балансов
            Тест: Запросом balancefix восстанавить баланс аккаунта и сравнить с изначальным (=0).
        Tear down: - Удалить аккаунты.
    """
    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins=101)
        assert "1" == balancefix['result']
        time.sleep(1)
        with allure.step('Запрашиваем новый баланс'):
            getaccountbalance_10 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
            assert float(0) == float(getaccountbalance_10['balance'])


@allure.title("Запрос balancefix: проверка работы - два логина.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_balancefix_2(accounts):
    """
    Описание шагов:
        Setup: Создание аккаунтов 101 и 102.
                - Пополение балансов аккаунтам на 10.000
                - Удаление ордеров о пополнении балансов
        Тест: запросом balancefix восстанавливаем баланс у аккаунтов и сравниваем с изначальным (0).
    Tear down: - аккаунты удаляются
    """
    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins="101;102")
        assert "1" == balancefix['result']
        time.sleep(1)
        with allure.step('Запрашиваем новый баланс'):
            getaccountbalance_10 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
            getaccountbalance_11 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=102)
            assert float(0) == float(getaccountbalance_10['balance']) and float(0) == float(getaccountbalance_11['balance'])
        time.sleep(1)

@allure.title("Запрос balancefix: несуществующий аккаунт.")
@allure.severity('minor')
def test_balancefix_3():
    """
    Описание шагов:
        Setup: Не требуется
        Тест: отправляем запрос balancefix с несуществующим аккаунтом
        Ожидаемый ответ: result=-4&logins_notfound=103&users=0&size=0&request_id=
    Tear down: -
    """
    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins=103)
        assert "-4" == balancefix['result'] and "103" == balancefix['logins_notfound']



@allure.title("Запрос balancefix: в перечне есть несуществующий аккаунт.")
@allure.severity('critical')
def test_balancefix_4(accounts):
    """
    Описание шагов:
        Setup: Создание аккаунтов 101 и 102.
                - Пополение балансов аккаунтам на 10.000
                - Удаление ордеров о пополнении балансов
        Тест:
            Шаг 1: запросом balancefix восстанавливаем баланс аккаунтов 101,102,103 (в перечне несуществующий 103 акк)
            Ожидаемый ответ: result=1&logins_notfound=103&request_id=
            Шаг 2: запросом getaccountbalance сравниваем с изначальным (0).
    Tear down: - аккаунты удаляются
    """
    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins="101;102;103")
        assert "1" == balancefix['result'] and "103" == balancefix['logins_notfound']
        time.sleep(1)
        with allure.step('Запрашиваем новый баланс'):
            getaccountbalance_10 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=101)
            getaccountbalance_11 = wik4.request(wik_mt4_addr, 'getaccountbalance', login=102)
            assert float(0) == float(getaccountbalance_10['balance']) and float(0) == float(getaccountbalance_11['balance'])


@allure.title("Запрос balancefix: некорректные значения.")
@allure.severity('minor')
def test_balancefix_5():
    """
    Описание шагов:
        Setup: Не требуется
        Тест: отправляем запрос balancefix с logins=asd.
        Ожидаемый результат: result=-3&reason=invalid params&request_id=
    Tear down: -
    """
    reason = "invalid params"

    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins="asd")
        assert "-3" == balancefix['result'] and reason == balancefix['reason']


@allure.title("Запрос balancefix: в перечне есть некорректные значения.")
@allure.severity('critical')
def test_balancefix_6(accounts):
    """
    Описание шагов:
        Setup: Создание аккаунтов 101 и 102.
                - Пополение балансов аккаунтам на 10.000
                - Удаление ордеров о пополнении балансов
        Тест: запросом balancefix восстанавливаем баланс аккаунтов  101,102,asd
        Ожидаемый результат: result=-3&reason=invalid params&request_id=
    Tear down: - аккаунты удаляются
    """
    reason = "invalid params"

    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins="101;102;asd")
        assert "-3" == balancefix['result'] and reason == balancefix['reason']
        time.sleep(1)


@allure.title("Запрос balancefix: в перечне только несуществующие и некорректные значения.")
@allure.severity('minor')
def test_balancefix_7():
    """
    Описание шагов:
        Setup: не требуется.
        Тест: отправляем запросом balancefix с logins=asd;12.
        Ожидаемый результат: result=-3&reason=invalid params&request_id=
    Tear down: -
    """
    reason = "invalid params"

    with allure.step('Запрос balancefix'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', logins="asd;103")
        assert "-3" == balancefix['result'] and reason == balancefix['reason']


@allure.title("Запрос balancefix: некорректные обязательные параметры или их отсутствие.")
@allure.severity('minor')
def test_balancefix_8(accounts):
    """
    Описание шагов:
        Setup: Создание аккаунтов 101 и 102.
                - Пополение балансов аккаунтам на 10.000
                - Удаление ордеров о пополнении балансов
        Тест:
            Шаг1: отправляем запросом balancefix с ошибкой в параметре logins
            Шаг2: отправляем запросом balancefix без параметра logins
        Ожидаемый результат: result=-3&reason=invalid params&request_id=
    Tear down: - аккаунты удаляются
    """
    reason = "invalid params"

    with allure.step('Запрос balancefix с ошибкой в параметре logins'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix', lons=101)
        assert "-3" == balancefix['result'] and reason == balancefix['reason']
    with allure.step('Запрос balancefix без параметра logins'):
        balancefix = wik4.request(wik_mt4_addr, 'balancefix',)
        assert "-3" == balancefix['result'] and reason == balancefix['reason']
