import web_api_mt4 as wik4
import pytest
import time
import allure
import random
from config import wik_mt4_addr, wik_mt4_addr_secret



@pytest.fixture()
def accounts():
    with allure.step('Создаем аккаунт'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    with allure.step('Начисление баланса на аккаунт'):
        wik4.request(wik_mt4_addr, 'changebalance', login=101, value='100000', comment='test_closeorderby')
        time.sleep(.100)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)
        time.sleep(0.1)
    

@allure.title("Запрос closeorderby: Закрыть две противоположные позиции по одному и тому же финансовому инструменту")
@allure.severity('critical')
def test_closeorderby_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
               - Начислить баланс на аккаунт
        Тест:
            Шаг1: Отправить запрос addorder на открыте ордера cmd=0
            Шаг2: Отправить запрос addorder на открыте ордера cmd=1
            Шаг3: Отправить запрос closeorderby на ранее открытые ордера
        Tear down: удалить аккаунт
    """
    random_volume_1 = random.randint(200, 300)
    random_volume_2 = random.randint(100, 150)
    with allure.step('Запрос addorder на открыте двух ордеров на 101 аккаунте (cmd=0 и cmd=1'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume_1, cmd=0)
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume_2, cmd=1)
        print('Ответ Шаг1 и Шаг2:\n', addorder_1, addorder_2)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result'] and '1' == addorder_2['result']
    with allure.step('Запрос closeorderby раннее открытых ордеров'):
        closeorderby_1 = wik4.request(wik_mt4_addr, 'closeorderby', order=addorder_1['order'], orderby=addorder_2['order'])
        print('Ответ Шаг3:\n', closeorderby_1)
        with allure.step('Проверка ответа'):
            assert '1' == closeorderby_1['result']


@allure.title("Запрос closeorderby: Закрыть две противоположные позиции по разным финансовым инструментам")
@allure.severity('minor')
def test_closeorderby_2(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунт с логином 101
               - Начислить баланс на аккаунт
        Тест:
            Шаг1: Отправить запрос addorder на открыте ордера cmd=0
            Шаг2: Отправить запрос addorder на открыте ордера cmd=1
            Шаг3: Отправить запрос closeorderby на ранее открытые ордера
        Tear down: удалить аккаунт
    """
    random_volume_1 = random.randint(200, 300)
    random_volume_2 = random.randint(100, 150)
    with allure.step('Запрос addorder на открыте двух ордеров на 101 аккаунте (cmd=0 и cmd=1'):
        addorder_1 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="EURUSD", volume=random_volume_1, cmd=0)
        addorder_2 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol="GBPUSD", volume=random_volume_2, cmd=1)
        print('Ответ Шаг1 и Шаг2:\n', addorder_1, addorder_2)
        with allure.step('Проверка ответа'):
            assert '1' == addorder_1['result'] and '1' == addorder_2['result']
    with allure.step('Запрос closeorderby раннее открытых ордеров'):
        closeorderby_1 = wik4.request(wik_mt4_addr, 'closeorderby', order=addorder_1['order'], orderby=addorder_2['order'])
        print('Ответ Шаг3:\n', closeorderby_1)
        with allure.step('Проверка ответа'):
            assert '-3' == closeorderby_1['result']



