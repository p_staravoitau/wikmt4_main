import web_api_mt4 as wik4
import pytest
import time
import allure
from config import wik_mt4_addr


@pytest.fixture()
def create_delete_account():
    with allure.step('Создаем аккаунт c логином 101'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100,
                     password='123456Qw', password_investor='123456Qw')
        with allure.step('Начисление баланса на аккаунт'):
            wik4.request(wik_mt4_addr, 'changebalance', login=101, value='10000', comment='test_accounthavetrades')
    time.sleep(0.01)
    yield ()
    with allure.step('Удаление созданных аккаунтов'):
        time.sleep(1)
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)


@allure.title("Запрос accounthavetrades: проверка работы запроса.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_accounthavetrades_1(create_delete_account):
    """
    Описание шагов:
        Setup: - создаём аккаунт с логином 101
            - пополняем ему баланс на 10.000
        Тест:
            Шаг1: отправляем запрос accounthavetrades
            Ожидаемый результат: должен вернуться ответ:'result=1&login=101&trades=0&request_id='
            Шаг2: открываем на аккаунте ордер
            Шаг3:отправляем запрос accounthavetrades
            Ожидаемый результат: должен вернуться ответ:'result=1&login=101&trades=1&request_id='
        Tear down: Удаление созданного аккаунта.
    """
    with allure.step('Запрос accounthavetrades'):
        accounthavetrades_0 = wik4.request(wik_mt4_addr, 'accounthavetrades', login=101)
        with allure.step('Проверка ответа'):
            assert '0' == accounthavetrades_0['trades'] and '1' == accounthavetrades_0['result']
    with allure.step('Открываем ордер'):
        openorder_cmd0 = wik4.request(wik_mt4_addr, 'addorder', login=101, symbol='EURUSD', volume=100, cmd=0)
        time.sleep(1)
    with allure.step('Запрос accounthavetrades'):
        accounthavetrades_1 = wik4.request(wik_mt4_addr, 'accounthavetrades', login=101)
        with allure.step('Проверка ответа'):
            assert '1' == accounthavetrades_1['trades'] and '1' == accounthavetrades_1['result']


@allure.title("Запрос accounthavetrades: несуществующий аккаунт")
@allure.severity('minor')
def test_accounthavetrades_2():
    """
    Описание шагов:
        Setup: Не требуется.
        Тест:
            Шаг1: Запрос accounthavetrades с несуществующим аккаунтом
            Ожидаемый результат:должен вернуться ответ 'result=-4&reason=invalid user&request_id='
        Tear down: -
    """
    reason = "invalid user"
    with allure.step('Запрос accounthavetrades с несуществующим аккаунтом'):
        accounthavetrades_0 = wik4.request(wik_mt4_addr, 'accounthavetrades', login=101)
        with allure.step('Проверка ответа'):
            assert reason == accounthavetrades_0['reason'] and '-4' == accounthavetrades_0['result']


@allure.title("Запрос accounthavetrades: некорректный тип данных (отличный от int) в параметре 'login'")
@allure.severity('minor')
def test_accounthavetrades_3():
    """
    Описание шагов:
        Setup: Не требуется.
        Тест:
            Шаг1: Запрос accounthavetrades с login=asd
            Ожидаемый результат:должен вернуться ответ 'result=-3&reason=invalid incoming parameters&request_id='
        Tear down: -
    """
    reason = "invalid incoming parameters"
    with allure.step('Запрос accounthavetrades с login=asd'):
        accounthavetrades_0 = wik4.request(wik_mt4_addr, 'accounthavetrades', login='asd')
        print('Ответ Шаг1:\n', accounthavetrades_0)
        with allure.step('Проверка ответа'):
            assert reason == accounthavetrades_0['reason'] and '-3' == accounthavetrades_0['result']


@allure.title("Запрос accounthavetrades: некорректные обязательные параметры или их отсутствие.")
@allure.severity('minor')
def test_accounthavetrades_4():
    """
    Описание шагов:
        Setup: Не требуется.
        Тест:
            Шаг1: Запрос accounthavetrades с ошибкой в параметре login.
            Шаг2: Запрос accounthavetrades без параметра login.
            Ожидаемый результат: должен вернуться ответ 'result=-3&reason=invalid incoming parameters&request_id='
        Tear down: -
    """
    reason = "invalid incoming parameters"
    
    with allure.step('Запрос accounthavetrades с ошибкой в параметре login'):
        accounthavetrades_1 = wik4.request(wik_mt4_addr, 'accounthavetrades', logi=101)
        print('Ответ Шаг1:\n', accounthavetrades_1)
    with allure.step('Запрос accounthavetrades без параметра login'):
        accounthavetrades_2 = wik4.request(wik_mt4_addr, 'accounthavetrades')
        print('Ответ Шаг2:\n', accounthavetrades_2)
    with allure.step('Проверка ответа'):
        assert '-3' == accounthavetrades_1['result'] and reason == accounthavetrades_1['reason']
        assert '-3' == accounthavetrades_2['result'] and reason == accounthavetrades_2['reason']

