import web_api_mt4 as wik4
import pytest
import allure
import time
import random
import random_value as rand
from config import wik_mt4_addr, wik_mt4_addr_secret

@pytest.fixture()
def accounts():
    with allure.step('Создать аккаунты'):
        wik4.request(wik_mt4_addr, 'createaccount', login=101, name='QA', group='demoforex', leverage=100)
    with allure.step('Начислить баланс на аккаунты'):
        wik4.request(wik_mt4_addr, 'changebalance', login=101, value='10000', comment='test_openpending')
    time.sleep(.100)
    yield ()
    with allure.step('Удалить созданные аккаунты'):
        wik4.request(wik_mt4_addr, 'deleteaccount', login=101)


@allure.title("Запрос openorder: открытие ордера с обязательными параметрами")
@allure.severity('blocker')
@pytest.mark.smoke
def test_openorder_1(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунты 101
               - Начислить 10000 баланса на 101 аккаунт
        Тест:
            Шаг1: Вбрасываем котировку для EURUSD
            Шаг2: Отправить запрос openorder: все обязательные параметры
            Шаг3: Запрос getopenorders: получение ордеров на аккаунте
        Tear down: Удаление аккаунта
    """
    random_volume = rand.random_volume()
    random_price = round(random.uniform(1.1, 1.5), 5)
    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.00000, ask=1.00008)
        print('Ответ Шаг1:\n', pushquote_EURUSD)
        assert pushquote_EURUSD['result'] == '1' and 'success' == pushquote_EURUSD['reason']
        time.sleep(2)
    with allure.step('Запрос openorder со всеми обязательными параметрами'):
        openorder_1 = wik4.request(wik_mt4_addr, 'openorder', login=101, symbol='EURUSD', cmd=0,
                                     volume=random_volume, price=random_price)
        print('Ответ Шаг2:\n', openorder_1)
        assert openorder_1['result'] == '1'
    with allure.step('Запрос getopenorders: получение ордеров на аккаунте'):
        getopenorders_1 = wik4.request(wik_mt4_addr, 'getopenorders', login=101)
        print('Ответ Шаг3:\n', getopenorders_1)
        assert getopenorders_1['result'] == '1' and getopenorders_1['trades'] == '1'\
               and getopenorders_1['answer'][0]['order'] == openorder_1['order'] \
               and float(getopenorders_1['answer'][0]['open_price']) == float(random_price) \
               and getopenorders_1['answer'][0]['symbol'] == 'EURUSD' \
               and float(getopenorders_1['answer'][0]['sl']) == float(0) \
               and float(getopenorders_1['answer'][0]['tp']) == float(0) \
               and getopenorders_1['answer'][0]['comment'] == '' \
               and getopenorders_1['answer'][0]['cmd'] == '0'


@allure.title("Запрос openorder: открытие ордера с обязательными и доп. параметрами")
@allure.severity('blocker')
@pytest.mark.smoke
def test_openorder_2(accounts):
    """
    Описание шагов:
        Setup: - Создать аккаунты 101
               - Начислить 10000 баланса на 101 аккаунт
        Тест:
            Шаг1: Вбрасываем котировку для EURUSD
            Шаг2: Отправить запрос openorder: все обязательные + доп. параметры
            Шаг3: Запрос getopenorders: получение ордеров на аккаунте
        Tear down: Удаление аккаунта
    """
    random_volume = rand.random_volume()
    random_price = round(random.uniform(1.1, 1.5), 5)
    random_sl = round(random.uniform(0.9, 1), 5)
    random_tp = round(random.uniform(1.3, 2), 5)
    with allure.step('Вбрасываем котировку'):
        pushquote_EURUSD = wik4.request(wik_mt4_addr_secret, 'pushquote', symbol='EURUSD', bid=1.00000, ask=1.00008)
        print('Ответ Шаг1:\n', pushquote_EURUSD)
        assert pushquote_EURUSD['result'] == '1' and 'success' == pushquote_EURUSD['reason']
        time.sleep(2)
    with allure.step('Запрос openorder со всеми обязательными параметрами'):
        openorder_1 = wik4.request(wik_mt4_addr, 'openorder', login=101, symbol='EURUSD', cmd=0,
                                     volume=random_volume, price=random_price, sl=random_sl, tp=random_tp, comment='test_openorder')
        print('Ответ Шаг2:\n', openorder_1)
        assert openorder_1['result'] == '1'
    with allure.step('Запрос getpendingorders: получение pending ордеров на аккаунте'):
        getopenorders_1 = wik4.request(wik_mt4_addr, 'getopenorders', login=101)
        print('Ответ Шаг3:\n', getopenorders_1)
        assert getopenorders_1['result'] == '1' and getopenorders_1['trades'] == '1'\
               and getopenorders_1['answer'][0]['order'] == openorder_1['order'] \
               and float(getopenorders_1['answer'][0]['open_price']) == float(random_price) \
               and getopenorders_1['answer'][0]['symbol'] == 'EURUSD' \
               and float(getopenorders_1['answer'][0]['sl']) == float(random_sl) \
               and float(getopenorders_1['answer'][0]['tp']) == float(random_tp) \
               and getopenorders_1['answer'][0]['comment'] == 'test_openorder'
