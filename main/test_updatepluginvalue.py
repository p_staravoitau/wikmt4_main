import web_api_mt4 as wik4
import pytest
import allure
import time
from config import wik_mt4_addr, wik_mt4_addr_secret

@pytest.fixture()
def delpluginparam():
    with allure.step('Запрос getplugin + deletepluginvalue: получаем текущие параметры плагина и полностью их удаляем'):
        getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        while getplugin_1['answer'][0]['parameter_name'] != '':
            wik4.request(wik_mt4_addr, 'deletepluginvalue', name='uber_feed.dll', param=getplugin_1['answer'][0]['parameter_name'])
            time.sleep(.200)
            getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
    yield ()


@allure.title("Запрос updatepluginvalue: добавление нового параметра")
@allure.severity('blocker')
@pytest.mark.smoke
def test_updatepluginvalue_1(delpluginparam):
    """
    Описание шагов:
        Setup: - Получить имеющиеся параметры плагина 'uber_feed.dll'
               - Удалить все параметры плагина 'uber_feed.dll'
        Тест:
            Шаг1: Отправить запрос updatepluginvalue с param='Test123' и value='true'
            Шаг2: Отправить запрос getplugin: проверяем, что параметр был добавлен
        Tear down: -
    """
    with allure.step('Запрос updatepluginvalue на добавление параметра в uber_feed.dll'):
        updatepluginvalue_1 = wik4.request(wik_mt4_addr, 'updatepluginvalue', name='uber_feed.dll', value='true',
                                            param='Test123')
        print('Ответ Шаг1:\n', updatepluginvalue_1)
        assert updatepluginvalue_1['result'] == '1'
    with allure.step('Запрос getplugin: получаем текущие параметры плагина'):
        getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        print('Ответ Шаг2:\n', getplugin_1)
        assert getplugin_1['result'] == '1' and getplugin_1['answer'][0]['parameter_name'] == 'Test123' \
               and getplugin_1['answer'][0]['value'] == 'true'


@allure.title("Запрос updatepluginvalue: обновление существующего")
@allure.severity('blocker')
@pytest.mark.smoke
def test_updatepluginvalue_2(delpluginparam):
    """
    Описание шагов:
        Setup: - Получить имеющиеся параметры плагина 'uber_feed.dll'
               - Удалить все параметры плагина 'uber_feed.dll'
        Тест:
            Шаг1: Отправить запрос updatepluginvalue с param='Test123' и value='true'
                  Отправить запрос updatepluginvalue с param='Test456' и value='false'
            Шаг2: Отправить запрос getplugin: проверяем, что параметры были добавлены
            Шаг3: Запрос updatepluginvalue на изменение существующего параметра 'Test123' в uber_feed.dll
            Шаг4: Проверка, что значение существующего параметра изменено
        Tear down: -
    """
    with allure.step('Запрос updatepluginvalue на добавление параметра в uber_feed.dll'):
        updatepluginvalue_1 = wik4.request(wik_mt4_addr, 'updatepluginvalue', name='uber_feed.dll', value='true',
                                            param='Test123')
        updatepluginvalue_2 = wik4.request(wik_mt4_addr, 'updatepluginvalue', name='uber_feed.dll', value='false',
                                           param='Test456')
        print('Ответ Шаг1:\n', updatepluginvalue_1, updatepluginvalue_2)
        assert updatepluginvalue_1['result'] == '1'
    with allure.step('Запрос getplugin: получаем текущие параметры плагина'):
        getplugin_1 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        print('Ответ Шаг2:\n', getplugin_1)
        assert getplugin_1['result'] == '1' and getplugin_1['answer'][0]['parameter_name'] == 'Test123' \
               and getplugin_1['answer'][0]['value'] == 'true' and getplugin_1['answer'][1]['parameter_name'] == 'Test456' \
               and getplugin_1['answer'][1]['value'] == 'false'
    with allure.step('Запрос updatepluginvalue на изменение существующего параметра в uber_feed.dll'):
        updatepluginvalue_3 = wik4.request(wik_mt4_addr, 'updatepluginvalue', name='uber_feed.dll', value='false',
                                            param='Test123')
        print('Ответ Шаг3:\n', updatepluginvalue_3)
        assert updatepluginvalue_1['result'] == '1'
    with allure.step('Запрос getplugin: получаем текущие параметры плагина'):
        getplugin_2 = wik4.request(wik_mt4_addr, 'getplugin', name='uber_feed.dll')
        print('Ответ Шаг4:\n', getplugin_2)
        assert getplugin_2['result'] == '1' and getplugin_2['answer'][0]['parameter_name'] == 'Test123' \
               and getplugin_2['answer'][0]['value'] == 'false' and getplugin_2['answer'][1]['parameter_name'] == 'Test456' \
               and getplugin_2['answer'][1]['value'] == 'false'

