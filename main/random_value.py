import random
from string import ascii_letters, digits

# Рандомный пароль для аккаунта МТ4
def acc_password(size):
    symbols = ascii_letters + digits

    # Преобразуем получившуюся строку в список
    ls = list(symbols)
    # Тщательно перемешиваем список
    random.shuffle(ls)
    # Извлекаем из списка 12 произвольных значений
    psw = ''.join([random.choice(ls) for x in range(size)])
    # Пароль готов
    return psw
    #acc_password (random.randint(6, 10))


# Рандомная группа для МТ4
def random_group(size):
    symbols = ascii_letters + digits

    # Преобразуем получившуюся строку в список
    ls = list(symbols)
    # Тщательно перемешиваем список
    random.shuffle(ls)
    # Извлекаем из списка 'size' произвольных значений
    group = ''.join([random.choice(ls) for x in range(size)])
    # Название группы готово
    return group

# Рандомное значение для ордера (1 lot = 100 для МТ4)
def random_volume():
    random_volume_order = random.randint(1, 1000)
    return random_volume_order

def random_amount(range1, range2):
    amount = round(random.uniform(range1, range2), 2)
    return amount

def random_price_order():
        price = random_volume()