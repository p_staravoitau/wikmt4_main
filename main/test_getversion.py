import web_api_mt4 as wik4
import allure
import pytest
from config import wik_mt4_addr
from config import version


@allure.title("Запрос getversion: проверка работы.")
@allure.severity('blocker')
@pytest.mark.smoke
def test_getversion_1():
    """
    Описание шагов:
        Setup: -
        Тест: У запроса нет параметров. В ответе возвразает версию Web-Api.
        Ожидаемый ответ: result=1&version=104&request_id=
        *версия зависит от сборки. перед запуском теста данные необходимо заменить
        Tear down: -
        """
    with allure.step('Запрос  getversion'):
        getversion = wik4.request(wik_mt4_addr, 'getversion')
        with allure.step('Проверка ответа'):
            assert '1' == getversion['result'] and version == getversion['version']