
"""Данные для подключения к WIK"""
wik_mt4_ip = '127.0.0.1'
wik_mt4_port = 12347
wik_mt4_addr = (wik_mt4_ip, wik_mt4_port)

"""Данные для подключения к WIK секретная сборка"""
wik_mt4_ip_secret = '127.0.0.1'
wik_mt4_port_secret = 12333
wik_mt4_addr_secret = (wik_mt4_ip_secret, wik_mt4_port_secret)

"""Данные для запроса getversion"""
version = "104"
